#include <QSharedPointer>
#include <QString>
#include <QtTest>

#include "correlation.h"

Q_DECLARE_METATYPE(QVector<int>)
Q_DECLARE_METATYPE(QVector<double>)

class CorrelationTest : public QObject
{
    Q_OBJECT

public:
    CorrelationTest();

private:
    static auto constexpr MAX_ERROR = 0.0000001;

private Q_SLOTS:
    void testConstructTheoreticCorrelation();

    void testWhiteNoise_data();
    void testWhiteNoise();

    void testExponential_data();
    void testExponential();

//    void testExperimentalCorrelation_data();
//    void testExperimentalCorrelation();
};

CorrelationTest::CorrelationTest()
{

}

void CorrelationTest::testConstructTheoreticCorrelation()
{
    TheoreticCorrelationWhiteNoise  wn;
    TheoreticCorrelationTriangular  tr;
    TheoreticCorrelationExponential exp;

    auto a = new TheoreticCorrelationWhiteNoise(0.1, 3 * M_PI, 5);

    QVERIFY(0.1 == a->deltaT());
    QVERIFY(5 == a->densityMaximum());
    QVERIFY(fabs(3 * M_PI - a->cutOffFrequency()) <= CorrelationTest::MAX_ERROR);

    delete a;
}

void CorrelationTest::testWhiteNoise_data()
{
    QTest::addColumn<double>("deltaT");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<int> >("range");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << 0.1 << 3 * M_PI << 5.0
                       << QVector<double>({0, 1, 2, 3, 4, 5, 6})
                       << QVector<double>({14.999999999999998, 12.875905370012095, 7.5682672864065692, 1.6393860718057747, -2.3387232094715973, -3.1830988618379066, -1.5591488063143966});
    QTest::newRow("2") << 0.1 << 3 * M_PI << 5.0
                       << QVector<double>({-3, -2, -1, 0, 1, 2, 3})
                       << QVector<double>{1.6393860718057747, 7.5682672864065692, 12.875905370012095, 14.999999999999998, 12.875905370012095, 7.5682672864065692, 1.6393860718057747};
}

void CorrelationTest::testWhiteNoise()
{
    QFETCH(double, deltaT);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);

    auto correlation = new TheoreticCorrelationWhiteNoise(deltaT, cutOffFrequency, densityMaximum);
    ComputeCorrelationStrategy computer(correlation);

    QFETCH(QVector<double>, range);
    QFETCH(QVector<double>, expected);

    auto actual = computer.call(range);

    auto sumError = 0.0;
    for (auto i = 0; i < actual.size(); i++)
        sumError += fabs(expected[i] - actual[i]);

    QVERIFY(actual.size() * CorrelationTest::MAX_ERROR >= sumError);
}

void CorrelationTest::testExponential_data()
{
    QTest::addColumn<double>("deltaT");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<int> >("range");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << 0.05 << M_PI/2 << 10.0
                       << QVector<double>({0, 1, 2, 3, 4, 5, 6})
                       << QVector<double>{10.0, 9.244652503762559, 8.546359991532334, 7.900812829377555, 7.304026910486456, 6.752319066557773, 6.242284336485698};
    QTest::newRow("2") << 0.05 << M_PI/2 << 10.0
                       << QVector<double>({-3, -2, -1, 0, 1, 2, 3})
                       << QVector<double>{7.900812829377555, 8.546359991532334, 9.244652503762559, 10.0, 9.244652503762559, 8.546359991532334, 7.900812829377555};
}

void CorrelationTest::testExponential()
{
    QFETCH(double, deltaT);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);

    auto correlation = new TheoreticCorrelationExponential(deltaT, cutOffFrequency, densityMaximum);
    ComputeCorrelationStrategy computer(correlation);

    QFETCH(QVector<double>, range);
    QFETCH(QVector<double>, expected);

    auto actual = computer.call(range);

    auto sumError = 0.0;
    for (auto i = 0; i < actual.size(); i++)
        sumError += fabs(expected[i] - actual[i]);

    QVERIFY(actual.size() * CorrelationTest::MAX_ERROR >= sumError);
}

//void CorrelationTest::testExperimentalCorrelation_data()
//{
//    QTest::addColumn<QVector<double> >("signal");
//    QTest::addColumn<QVector<int> >("range");
//    QTest::addColumn<QVector<double> >("expected");

//    QTest::newRow("1") << QVector<double>{1.1, 1.7, 14.6, 11., 8.9}
//                       << QVector<int>{0, 1, 2, 3, 4, 5, 6}
//                       << QVector<double>{15.82347748, 10.80963313, 6.24266831, 1.032106, 0.37107};
//    QTest::newRow("2") << QVector<double>{}
//                       << QVector<int>{-3, -2, -1, 0, 1, 2, 3}
//                       << QVector<double>{};
//    QTest::newRow("1") << QVector<double>{1.1, 1.7, 14.6, 11., 8.9}
//                       << QVector<int>{0, 1, 2, 3, 4, 5, 6}
//                       << QVector<double>{};
//    QTest::newRow("2") << QVector<double>{}
//                       << QVector<int>{-3, -2, -1, 0, 1, 2, 3}
//                       << QVector<double>{};
//}

//void CorrelationTest::testExperimentalCorrelation()
//{
//    QFETCH(QVector<double>, signal);
//    QFETCH(QVector<int>, range);
//    QFETCH(QVector<double>, expected);

//    ComputeCorrelationStrategy computer(new ExperimentalCorrelation(signal));

//    auto actual = computer.call(range);

//    auto sumError = 0.0;
//    for (auto i = 0; i < actual.size(); i++)
//        sumError += fabs(expected[i] - actual[i]);

//    QVERIFY(CorrelationTest::MAX_ERROR >= sumError);
//}

QTEST_APPLESS_MAIN(CorrelationTest)

#include "correlation_test.moc"
