#include <QSharedPointer>
#include <QString>
#include <QtTest>

#include "density.h"

Q_DECLARE_METATYPE(QSharedPointer<AbstractDensity>)


class DensityTest : public QObject
{
    Q_OBJECT

public:
    DensityTest();

private Q_SLOTS:
    void testDensityParams_data();
    void testDensityParams();

    void testWhiteNoiseDensityEmpty();

    void testWhiteNoiseDensity_data();
    void testWhiteNoiseDensity();

    void testTriangularDensityEmpty();

    void testTriangularDensity_data();
    void testTriangularDensity();

    void testExponentialDensityEmpty();

    void testExponentialDensity_data();
    void testExponentialDensity();

private:
    /**
     * @brief Максимально допустимая погрешность вычислений
     * для чисел с плавающей точкой.
     */
    static constexpr auto EPSILON = 0.0000001;
};

DensityTest::DensityTest()
{

}

void DensityTest::testDensityParams_data()
{
    QTest::addColumn<QSharedPointer<AbstractDensity> >("density");
    QTest::addColumn<double>("cutOffFreqConstructor");
    QTest::addColumn<double>("maxDensityConstructor");
    QTest::addColumn<double>("cutOffFreqSetter");
    QTest::addColumn<double>("maxDensitySetter");

    QTest::newRow("1") << QSharedPointer<AbstractDensity>(new WhiteNoiseDensity(1.0, 1.2))    << 1.0   << 1.2  << 3.4 << 5.6;
    QTest::newRow("2") << QSharedPointer<AbstractDensity>(new WhiteNoiseDensity(-145., -0.0)) << -145. << -0.0 << 9.1 << -15.6;
    QTest::newRow("3") << QSharedPointer<AbstractDensity>(new TriangularDensity(1.0, 1.2))    << 1.0   << 1.2  << 3.4 << 5.6;
    QTest::newRow("4") << QSharedPointer<AbstractDensity>(new TriangularDensity(-145., -0.0)) << -145. << -0.0 << 9.1 << -15.6;
    QTest::newRow("5") << QSharedPointer<AbstractDensity>(new WhiteNoiseDensity(1.0, 1.2))    << 1.0   << 1.2  << 3.4 << 5.6;
    QTest::newRow("6") << QSharedPointer<AbstractDensity>(new WhiteNoiseDensity(-145., -0.0)) << -145. << -0.0 << 9.1 << -15.6;
}

void DensityTest::testDensityParams()
{
    QFETCH(QSharedPointer<AbstractDensity>, density);

    QFETCH(double, cutOffFreqConstructor);
    QFETCH(double, maxDensityConstructor);

    QVERIFY(fabs(cutOffFreqConstructor) == density->cutOffFrequency());
    QVERIFY(fabs(maxDensityConstructor) == density->maximum());

    QFETCH(double, cutOffFreqSetter);
    QFETCH(double, maxDensitySetter);

    density->setCutOffFrequency(cutOffFreqSetter);
    QVERIFY(fabs(cutOffFreqSetter) == density->cutOffFrequency());

    density->setMaximum(maxDensitySetter);
    QVERIFY(fabs(maxDensitySetter) == density->maximum());
}

void DensityTest::testWhiteNoiseDensityEmpty()
{
    WhiteNoiseDensity density;

    QVERIFY(0 == density.cutOffFrequency());
    QVERIFY(0 == density.maximum());
}

void DensityTest::testWhiteNoiseDensity_data()
{
    QTest::addColumn<double>("cuttOffFreq");
    QTest::addColumn<double>("maxDensity");
    QTest::addColumn<double>("expectedDensity");
    QTest::addColumn<double>("argument");

    QTest::newRow("1") << 0.1      << 10.0 << 0.0  << - 9.4;
    QTest::newRow("2") << 7.0      << 10.0 << 10.0 << - 7.0 ;
    QTest::newRow("3") << 7.0      << 20.0 << 20.0 << - 0.771 ;
    QTest::newRow("4") << M_PI_2   << 1.0  << 1.0  << 0.0;
    QTest::newRow("5") << M_PI     << 10.0 << 10.0 << 0.5;
    QTest::newRow("6") << 5 * M_PI << 20.0 << 20.0 << 5 * M_PI;
}

void DensityTest::testWhiteNoiseDensity()
{
    QFETCH(double, cuttOffFreq);
    QFETCH(double, maxDensity);

    WhiteNoiseDensity density(cuttOffFreq, maxDensity);

    QFETCH(double, expectedDensity);
    QFETCH(double, argument);

    QVERIFY(expectedDensity == density(argument));
}

void DensityTest::testTriangularDensityEmpty()
{
    TriangularDensity density;

    QVERIFY(0 == density.cutOffFrequency());
    QVERIFY(0 == density.maximum());
}

void DensityTest::testTriangularDensity_data()
{
    QTest::addColumn<double>("cuttOffFreq");
    QTest::addColumn<double>("maxDensity");
    QTest::addColumn<double>("expectedDensity");
    QTest::addColumn<double>("argument");

    QTest::newRow("w negative, abs more than cuttOffFreq") << 0.1      << 10.0 << 0.0               << - 9.4;
    QTest::newRow("w negative, abs eq to cuttOffFreq")     << 7.0      << 10.0 << 20.0              << - 7.0 ;
    QTest::newRow("w negative, abs less than cuttOffFreq") << 7.0      << 20.0 << 22.20285714285714 << - 0.771 ;
    QTest::newRow("w zero")                                << M_PI_2   << 1.0  << 1.0               << 0.0;
    QTest::newRow("w less than cuttOffFreq")               << M_PI     << 10.0 << 8.408450569081047 << 0.5;
    QTest::newRow("w eq to cuttOffFreq")                   << 5 * M_PI << 20.0 << 0.0               << 5 * M_PI;
}

void DensityTest::testTriangularDensity()
{
    QFETCH(double, cuttOffFreq);
    QFETCH(double, maxDensity);

    TriangularDensity density(cuttOffFreq, maxDensity);

    QFETCH(double, expectedDensity);
    QFETCH(double, argument);

    QVERIFY(fabs(expectedDensity - density(argument)) < DensityTest::EPSILON);
}

void DensityTest::testExponentialDensityEmpty()
{
    ExponentialDensity density;

    QVERIFY(0 == density.cutOffFrequency());
    QVERIFY(0 == density.maximum());
}

void DensityTest::testExponentialDensity_data()
{
    QTest::addColumn<double>("cuttOffFreq");
    QTest::addColumn<double>("dispersion");
    QTest::addColumn<double>("expectedDensity");
    QTest::addColumn<double>("argument");

    QTest::newRow("w negative")    << M_PI << 20.0     << 0.233198727701959 << -23.0;
    QTest::newRow("w zero")        << M_PI << 20.0     << 12.73239544735163 << 0.0;
    QTest::newRow("w = 1")         << M_PI << 45.0     << 27.94015730423657 << 0.5;
    QTest::newRow("w = 22.4")      << M_PI << 0.15     << 0.001842109580778 << 22.4;
    QTest::newRow("w = 134.22818") << M_PI << 5 * M_PI << 0.005474878757408 << 134.22818;
}

void DensityTest::testExponentialDensity()
{
    QFETCH(double, cuttOffFreq);
    QFETCH(double, dispersion);

    ExponentialDensity density(cuttOffFreq, dispersion);

    QFETCH(double, expectedDensity);
    QFETCH(double, argument);

    QVERIFY(fabs(expectedDensity - density(argument)) < DensityTest::EPSILON);
}

QTEST_APPLESS_MAIN(DensityTest)

#include "density_test.moc"
