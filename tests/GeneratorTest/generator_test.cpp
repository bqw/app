#include <QString>
#include <QtTest>

#include <iomanip>
#include <iostream>

#include "generator.h"


class GeneratorTest : public QObject
{
    Q_OBJECT

public:
    GeneratorTest();

private Q_SLOTS:
    void testLambdaGeneratorNormal_data();
    void testLambdaGeneratorNormal();

    void testMuGenerator();

private:
    template <class T>
    static double __computeMean(const QVector<T> &rhs);
    template <class T>
    static double __computeStd(const QVector<T> &rhs, double mean = 0.0);

    static constexpr auto EPSILON = 5.0;
};

GeneratorTest::GeneratorTest()
{

}

void GeneratorTest::testLambdaGeneratorNormal_data()
{
    QTest::addColumn<double>("mu");
    QTest::addColumn<double>("sigma");
    QTest::addColumn<int>("length");

    QTest::newRow("0, 1")        << 0.0   << 1.0   << 10000;
    QTest::newRow("14.88, 22.3") << 14.88 << 22.3  << 10000;
    QTest::newRow("0, 100")      << 0.0   << 100.0 << 10000;
}

void GeneratorTest::testLambdaGeneratorNormal()
{
    QFETCH(double, mu);
    QFETCH(double, sigma);
    QFETCH(int, length);

    LambdaGeneratorNormal generator(mu, sigma);

    auto generated = generator.get(length);

//    std::cout << fabs(mu - GeneratorTest::__computeMean<double>(generated)) << std::endl;

    QVERIFY(length == generated.length());
    QVERIFY(fabs(mu - GeneratorTest::__computeMean<double>(generated)) < GeneratorTest::EPSILON);
}

void GeneratorTest::testMuGenerator()
{
    MuGeneratorUniform generator;

    auto generated = generator(10);

    QVector<int> mu;
    QVector<int> gamma;

    for (auto it = generated.cbegin(); it != generated.cend(); it++) {
        mu    << it->first;
        gamma << it->second;
    }

//    std::cout << __computeMean<int>(mu) << " " << __computeMean<int>(gamma) << std::endl;

    QVERIFY(0 == __computeMean<int>(mu));
    QVERIFY(0 == __computeMean<int>(gamma));

//    std::cout << __computeStd<int>(mu) << " " << __computeStd<int>(gamma) << std::endl;

    QVERIFY(1 == __computeStd<int>(mu));
    QVERIFY(1 == __computeStd<int>(gamma));

//    for (auto &g : generated)
//        std::cout << g.first << " " << g.second << std::endl;
}

template <class T>
double GeneratorTest::__computeMean(const QVector<T> &rhs)
{
    return std::accumulate(rhs.begin(), rhs.end(), T(0)) / rhs.length();
}

template <class T>
double GeneratorTest::__computeStd(const QVector<T> &rhs, double mean)
{
    return std::accumulate(rhs.cbegin(), rhs.cend(), T(0),
                           [mean](double init, double elem){ return init + pow(elem - mean, 2); }) / rhs.length();
}

QTEST_APPLESS_MAIN(GeneratorTest)

#include "generator_test.moc"
