#include <QString>
#include <QtTest>

#include "bitset.h"


Q_DECLARE_METATYPE(BitSet)


class BitSetTest : public QObject
{
    Q_OBJECT

public:
    BitSetTest();

private Q_SLOTS:
    void testConstructors();

    void testBits_data();
    void testBits();
};

BitSetTest::BitSetTest()
{
}

void BitSetTest::testConstructors()
{
    auto bs1 = BitSet();
    auto bs2 = BitSet(10, 6);

    auto bs3 = BitSet(bs2);

    QVERIFY(10 == bs3.value());
    QVERIFY(6 == bs3.size());

    auto bs4 = BitSet(std::move(bs3));

    QVERIFY(10 == bs4.value());
    QVERIFY(6 == bs4.size());

    auto bs5 = bs4;

    QVERIFY(10 == bs5.value());
    QVERIFY(6 == bs5.size());

    auto bs6 = std::move(bs4);

    QVERIFY(10 == bs6.value());
    QVERIFY(6 == bs6.size());
}

void BitSetTest::testBits_data()
{
    QTest::addColumn<int>("value");
    QTest::addColumn<int>("size");
    QTest::addColumn<BitSet>("bitset");

    QTest::newRow("test value fits size") << 3  << 4 << BitSet(3, 4);
    QTest::newRow("test value > size")    << 16 << 2 << BitSet(16, 2);
}

void BitSetTest::testBits()
{
    QFETCH(int, value);
    QFETCH(int, size);

    QFETCH(BitSet, bitset);

    QVERIFY(static_cast<std::size_t>(value) == bitset.value());
    QVERIFY(size == bitset.size());
    QVERIFY(static_cast<std::size_t>(size) == bitset.length());

    for (auto i = 0; i < size; i++) {
        QVERIFY(value % 2 == bitset.at(i));
        value /= 2;
    }
}

QTEST_APPLESS_MAIN(BitSetTest)

#include "bit_set_test.moc"
