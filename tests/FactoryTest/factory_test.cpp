#include <QString>
#include <QtTest>

#include "factory.h"

class FactoryTest : public QObject
{
    Q_OBJECT

public:
    FactoryTest();

private Q_SLOTS:
    void testDensityFactories();
    void testBasesFactories();
    void testTheoreticCorrelationFactories();
    void testAlgorithmicCorrelationFactories();
    void testFourierTransformFactories();
};

FactoryTest::FactoryTest()
{

}

void FactoryTest::testDensityFactories()
{
    WhiteNoiseDensityFactory  wnf;
    ExponentialDensityFactory expf;
    TriangularDensityFactory  tf;

    auto pwn  = wnf.create();
    auto pexp = expf.create();
    auto pt   = tf.create();

    QVERIFY(nullptr != dynamic_cast<WhiteNoiseDensity  *>(pwn.data()));
    QVERIFY(nullptr != dynamic_cast<ExponentialDensity *>(pexp.data()));
    QVERIFY(nullptr != dynamic_cast<TriangularDensity  *>(pt.data()));
}

void FactoryTest::testBasesFactories()
{
    TrigonometricBasisFactory tf;
    HadamardBasisFactory      hf;

    auto pt = tf.create();
    auto ph = hf.create();

    QVERIFY(nullptr != dynamic_cast<TrigonometricBasis *>(pt.data()));
    QVERIFY(nullptr != dynamic_cast<HadamardBasis*>(ph.data()));
}

void FactoryTest::testTheoreticCorrelationFactories()
{
    TheoreticCorrelationWhiteNoiseFactory  twnf;
    TheoreticCorrelationTriangularFactory  ttf;
    TheoreticCorrelationExponentialFactory texpf;

    auto pwn  = twnf.create();
    auto pt   = ttf.create();
    auto pexp = texpf.create();

    QVERIFY(nullptr != dynamic_cast<TheoreticCorrelationWhiteNoise *>(pwn.data()));
    QVERIFY(nullptr != dynamic_cast<TheoreticCorrelationTriangular *>(pt.data()));
    QVERIFY(nullptr != dynamic_cast<TheoreticCorrelationExponential *>(pexp.data()));
}

void FactoryTest::testAlgorithmicCorrelationFactories()
{
    AlgorithmicCorrelationSingleFactory asf;
    AlgorithmicCorrelationPairFactory   apf;

    auto ps = asf.create();
    auto pp = apf.create();

    QVERIFY(nullptr != dynamic_cast<AlgorithmicCorrelationSingle *>(ps.data()));
    QVERIFY(nullptr != dynamic_cast<AlgorithmicCorrelationPair *>(pp.data()));
}

void FactoryTest::testFourierTransformFactories()
{
    BackDftSingleFactory bsf;
    BackDftPairFactory   bpf;

    auto pbs = bsf.create();
    auto pbp = bpf.create();

    QVERIFY(nullptr != dynamic_cast<BackDftSingle *>(pbs.data()));
    QVERIFY(nullptr != dynamic_cast<BackDftPair *>(pbp.data()));
}

QTEST_APPLESS_MAIN(FactoryTest)

#include "factory_test.moc"
