TEMPLATE = subdirs

SUBDIRS +=         \
    BasesTest      \
    DensityTest    \
    GeneratorTest  \
    SpectrumTest   \
    CorrelationTest \
    FourierTest

INCLUDEPATH += ../libs/imitation_lib
DEPENDPATH  += $${INCLUDEPATH}

# link against UILib
_IMITATION_LIB = ../UIProject/
CONFIG(debug, debug|release) {
    win32: _IMITATION_LIB = $$join(_IMITATION_LIB,,,debug/UIProject.lib)
} else {
    win32: _IMITATION_LIB = $$join(_IMITATION_LIB,,,release/UIProject.lib)
}
LIBS += $${_IMITATION_LIB}
PRE_TARGETDEPS += $${_IMITATION_LIB}
