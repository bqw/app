#include <QSharedPointer>
#include <QString>
#include <QtTest>

#include "fourier.h"


Q_DECLARE_METATYPE(QSharedPointer<StationaryBackHadamardTransfrom>)
Q_DECLARE_METATYPE(QSharedPointer<NonstationaryBackHadamardTransform>)
Q_DECLARE_METATYPE(QVector<double>)


class FourierTest : public QObject
{
    Q_OBJECT

public:
    FourierTest();

private Q_SLOTS:
    void testBaseClassConstruct();

//    void testBaseClassConstructParams_data();
//    void testBaseClassConstructParams();

    void testStationaryConstruct();

//    void testStationaryParams_data();
//    void testStationaryParams();

    void testNonstationaryConstruct();

//    void testNonstationaryParams_data();
//    void testNonstationaryParams();

    void testHadamardStationaryConstruct();

//    void testHadamardStationaryParams_data();
//    void testHadamardStationaryParams();

    void testHadamardNonstationaryConstruct();

//    void testHadamardNonstationaryParams_data();
//    void testHadamardNonstationaryParams();

    void testNonstationary_data();
    void testNonstationary();

    void testStationary_data();
    void testStationary();
};

FourierTest::FourierTest()
{

}

void FourierTest::testBaseClassConstruct()
{
    auto b = new AbstractBackFourierTransform();
    delete b;
}

void FourierTest::testStationaryConstruct()
{
    auto s = new StationaryBackFourierTransform();
    delete s;
}

void FourierTest::testNonstationaryConstruct()
{
    auto n = new NonstationaryBackFourierTransform();
    delete n;
}

void FourierTest::testHadamardStationaryConstruct()
{
    auto hs = new StationaryBackHadamardTransfrom();

    QVERIFY(0 == hs->basis()->N());

    hs->setN(32);

    QVERIFY(32 == hs->basis()->N());

    delete hs;
}

void FourierTest::testHadamardNonstationaryConstruct()
{
    auto hn = new NonstationaryBackHadamardTransform();

    QVERIFY(0 == hn->basis()->N());

    hn->setN(32);

    QVERIFY(32 == hn->basis()->N());

    delete hn;
}

void FourierTest::testNonstationary_data()
{
    QTest::addColumn<QSharedPointer<NonstationaryBackHadamardTransform> >("transform");
    QTest::addColumn<QVector<double> >("spectrum");

    QTest::newRow("1") << QSharedPointer<NonstationaryBackHadamardTransform>(new NonstationaryBackHadamardTransform(8, new MuGeneratorUniform))
                       << QVector<double>({1., 2., 3., 4., 5., 6., 7., 8.});
    QTest::newRow("2") << QSharedPointer<NonstationaryBackHadamardTransform>(new NonstationaryBackHadamardTransform(8))
                       << QVector<double>({1., 2., 3., 4., 5., 6., 7., 8.});
}

void FourierTest::testNonstationary()
{
    QFETCH(QSharedPointer<NonstationaryBackHadamardTransform>, transform);
    QFETCH(QVector<double>, spectrum);

    auto result1 = transform->call(spectrum);
    auto result2 = transform->call(spectrum, true);

    QVERIFY(!result1.isEmpty());
    QVERIFY(QVector<double>(result1.size(), 0) != result1);

    QVERIFY(!result2.isEmpty());
    QVERIFY(QVector<double>(result2.size(), 0) != result2);
}

void FourierTest::testStationary_data()
{
    QTest::addColumn<QSharedPointer<StationaryBackHadamardTransfrom> >("transform");
    QTest::addColumn<QVector<double> >("spectrum");

    QVector<double> spec = {1., 1., 1., 1., 1., 1., 1., 1.};

    QTest::newRow("1") << QSharedPointer<StationaryBackHadamardTransfrom>(new StationaryBackHadamardTransfrom(8, new MuGeneratorUniform))
                       << spec;
    QTest::newRow("2") << QSharedPointer<StationaryBackHadamardTransfrom>(new StationaryBackHadamardTransfrom(8))
                       << spec;
}

void FourierTest::testStationary()
{
    QFETCH(QSharedPointer<StationaryBackHadamardTransfrom>, transform);
    QFETCH(QVector<double>, spectrum);

    TransformSpectrumStrategy t(new TrigonometricBasis(spectrum.length()), new HadamardBasis(spectrum.length()));

    auto transformedSpec = t.call(spectrum);

    auto result1 = transform->call(std::move(transformedSpec));
    auto result2 = transform->call(std::move(transformedSpec));

    QVERIFY(!result1.isEmpty());
    QVERIFY(QVector<double>(result1.size(), 0) != result1);

    QVERIFY(!result2.isEmpty());
    QVERIFY(QVector<double>(result2.size(), 0) != result2);
}

QTEST_APPLESS_MAIN(FourierTest)

#include "fourier_test.moc"
