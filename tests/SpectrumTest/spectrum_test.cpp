#include <iostream>

#include <QSharedPointer>
#include <QString>
#include <QtTest>
#include <QVector>

#include "density.h"
#include "spectrum.h"


Q_DECLARE_METATYPE(QVector<double>)


class SpectrumTest : public QObject
{
    Q_OBJECT

public:
    SpectrumTest();

private Q_SLOTS:
    void testComputeSpectrumEmpty();

    void testComputeSpectrumWhiteNoise_data();
    void testComputeSpectrumWhiteNoise();

    void testComputeSpectrumTriangular_data();
    void testComputeSpectrumTriangular();

    void testComputeSpectrumExpenential_data();
    void testComputeSpectrumExpenential();

    void testComputeSpectrumHadamard_data();
    void testComputeSpectrumHadamard();

    void testComputeSpectrum_data();
    void testComputeSpectrum();

    void testTransfromSpectrum_data();
    void testTransfromSpectrum();
};

SpectrumTest::SpectrumTest()
{

}

void SpectrumTest::testComputeSpectrumEmpty()
{
    auto c = new ComputeSpectrumStrategy();
    delete c;
}

void SpectrumTest::testComputeSpectrumWhiteNoise_data()
{
    QTest::addColumn<double>("timeInterval");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<double> >("lambdas");

    QTest::newRow("1") << 10. << M_PI     << 10.  << QVector<double>();
    QTest::newRow("4") << 10. << M_PI / 2 << 0.9 << QVector<double>({1., 2., 3.});
}

void SpectrumTest::testComputeSpectrumWhiteNoise()
{
    QFETCH(double, timeInterval);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);
    QFETCH(QVector<double>, lambdas);

    auto density = new WhiteNoiseDensity(cutOffFrequency, densityMaximum);
    ComputeSpectrumStrategy c(timeInterval, density, lambdas);

    QVERIFY(timeInterval               == c.timeInterval());
    QVERIFY(density->cutOffFrequency() == c.density()->cutOffFrequency());
    QVERIFY(density->maximum()         == c.density()->maximum());

    auto deltaW = 2 * M_PI / timeInterval;
    auto deltaT = M_PI / density->cutOffFrequency();
    auto N      = timeInterval / deltaT;

    QVERIFY(deltaW == c.deltaW());
    QVERIFY(deltaT == c.deltaT());
    QVERIFY(N      == c.N());

    if (!lambdas.isEmpty()) {
        c.clearLambdas();
        QVERIFY(c.lambdas().isEmpty());
    }

    auto size = (lambdas.isEmpty()) ? 3 : lambdas.size();

    QVector<double> newLambdas;
    newLambdas.reserve(size);

    for (auto i = 0; i < size; i++)
        newLambdas << 11.;

    c.setLambdas(newLambdas);

    QVERIFY(lambdas    != c.lambdas());
    QVERIFY(newLambdas == c.lambdas());

    auto newDensity = new TriangularDensity(density->cutOffFrequency() + 11.8, density->maximum());

    c.setDensity(newDensity);

    auto newDeltaT = M_PI / newDensity->cutOffFrequency();
    auto newN      = static_cast<std::size_t>(floor(timeInterval / newDeltaT));

    QVERIFY(deltaT != c.deltaT());
    QVERIFY(N      != c.N());

    QVERIFY(newDeltaT == c.deltaT());
    QVERIFY(newN      == c.N());
}

void SpectrumTest::testComputeSpectrumTriangular_data()
{
    QTest::addColumn<double>("timeInterval");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<double> >("lambdas");

    QTest::newRow("1") << 10. << M_PI     << 10.  << QVector<double>();
    QTest::newRow("4") << 10. << M_PI / 2 << 0.9 << QVector<double>({1., 2., 3.});
}

void SpectrumTest::testComputeSpectrumTriangular()
{
    QFETCH(double, timeInterval);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);
    QFETCH(QVector<double>, lambdas);

    auto density = new TriangularDensity(cutOffFrequency, densityMaximum);
    ComputeSpectrumStrategy c(timeInterval, density, lambdas);

    QVERIFY(timeInterval               == c.timeInterval());
    QVERIFY(density->cutOffFrequency() == c.density()->cutOffFrequency());
    QVERIFY(density->maximum()         == c.density()->maximum());

    auto deltaW = 2 * M_PI / timeInterval;
    auto deltaT = M_PI / density->cutOffFrequency();
    auto N      = timeInterval / deltaT;

    QVERIFY(deltaW == c.deltaW());
    QVERIFY(deltaT == c.deltaT());
    QVERIFY(N      == c.N());

    if (!lambdas.isEmpty()) {
        c.clearLambdas();
        QVERIFY(c.lambdas().isEmpty());
    }

    auto size = (lambdas.isEmpty()) ? 3 : lambdas.size();

    QVector<double> newLambdas;
    newLambdas.reserve(size);

    for (auto i = 0; i < size; i++)
        newLambdas << 11.;

    c.setLambdas(newLambdas);

    QVERIFY(lambdas    != c.lambdas());
    QVERIFY(newLambdas == c.lambdas());

    auto newDensity = new TriangularDensity(density->cutOffFrequency() + 11.8, density->maximum());

    c.setDensity(newDensity);

    auto newDeltaT = M_PI / newDensity->cutOffFrequency();
    auto newN      = static_cast<std::size_t>(floor(timeInterval / newDeltaT));

    QVERIFY(deltaT != c.deltaT());
    QVERIFY(N      != c.N());

    QVERIFY(newDeltaT == c.deltaT());
    QVERIFY(newN      == c.N());
}

void SpectrumTest::testComputeSpectrumExpenential_data()
{
    QTest::addColumn<double>("timeInterval");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<double> >("lambdas");

    QTest::newRow("1") << 10. << M_PI     << 10.  << QVector<double>();
    QTest::newRow("4") << 10. << M_PI / 2 << 0.9 << QVector<double>({1., 2., 3.});
}

void SpectrumTest::testComputeSpectrumExpenential()
{
    QFETCH(double, timeInterval);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);
    QFETCH(QVector<double>, lambdas);

    auto density = new ExponentialDensity(cutOffFrequency, densityMaximum);
    ComputeSpectrumStrategy c(timeInterval, density, lambdas);

    QVERIFY(timeInterval               == c.timeInterval());
    QVERIFY(density->cutOffFrequency() == c.density()->cutOffFrequency());
    QVERIFY(density->maximum()         == c.density()->maximum());

    auto deltaW = 2 * M_PI / timeInterval;
    auto deltaT = M_PI / density->cutOffFrequency();
    auto N      = timeInterval / deltaT;

    QVERIFY(deltaW == c.deltaW());
    QVERIFY(deltaT == c.deltaT());
    QVERIFY(N      == c.N());

    if (!lambdas.isEmpty()) {
        c.clearLambdas();
        QVERIFY(c.lambdas().isEmpty());
    }

    auto size = (lambdas.isEmpty()) ? 3 : lambdas.size();

    QVector<double> newLambdas;
    newLambdas.reserve(size);

    for (auto i = 0; i < size; i++)
        newLambdas << 11.;

    c.setLambdas(newLambdas);

    QVERIFY(lambdas    != c.lambdas());
    QVERIFY(newLambdas == c.lambdas());

    auto newDensity = new TriangularDensity(density->cutOffFrequency() + 11.8, density->maximum());

    c.setDensity(newDensity);

    auto newDeltaT = M_PI / newDensity->cutOffFrequency();
    auto newN      = static_cast<std::size_t>(floor(timeInterval / newDeltaT));

    QVERIFY(deltaT != c.deltaT());
    QVERIFY(N      != c.N());

    QVERIFY(newDeltaT == c.deltaT());
    QVERIFY(newN      == c.N());
}

void SpectrumTest::testComputeSpectrumHadamard_data()
{
    QTest::addColumn<double>("timeInterval");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<int>("expectedN");

    QTest::newRow("1") << 300. << M_PI << 512;
}

void SpectrumTest::testComputeSpectrumHadamard()
{
    QFETCH(double, timeInterval);
    QFETCH(double, cutOffFrequency);
    QFETCH(int, expectedN);

    ComputeSpectrumStrategyHadamard h(timeInterval, new WhiteNoiseDensity(cutOffFrequency, 1.0));

    QVERIFY(expectedN == static_cast<int>(h.N()));
}

void SpectrumTest::testComputeSpectrum_data()
{
    QTest::addColumn<double>("timeInterval");
    QTest::addColumn<double>("cutOffFrequency");
    QTest::addColumn<double>("densityMaximum");
    QTest::addColumn<QVector<double> >("lambdas");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << 16.0 << M_PI / 2 << 10.0
                       << QVector<double>({})
                       << QVector<double>({0.7905694150420949, 0.5590169943749475, 0.5590169943749475, 0.5590169943749475, 0.5590169943749475, 0.5590169943749475, 0.5590169943749475, 0.5590169943749475});
}

void SpectrumTest::testComputeSpectrum()
{
    QFETCH(double, timeInterval);
    QFETCH(double, cutOffFrequency);
    QFETCH(double, densityMaximum);

    auto d = new WhiteNoiseDensity(cutOffFrequency, densityMaximum);

    QFETCH(QVector<double>, lambdas);

    ComputeSpectrumStrategyHadamard c(timeInterval, d, lambdas);

    QFETCH(QVector<double>, expected);

    auto result = c.call();

//    for (auto &a : result)
//        std::cout << a << " ";

    QVERIFY(expected == result);
}

void SpectrumTest::testTransfromSpectrum_data()
{
    QTest::addColumn<QVector<double> >("spectrum");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << QVector<double>({1., 1., 1., 1., 1., 1., 1., 1.})
                       << QVector<double>({1., 1., 1., 0., 1.207106, -0.207106, .5, .5});
}

void SpectrumTest::testTransfromSpectrum()
{
    QFETCH(QVector<double>, spectrum);

    TransformSpectrumStrategy t(new TrigonometricBasis(spectrum.length()), new HadamardBasis(spectrum.length()));

    QFETCH(QVector<double>, expected);

    auto result = t.call(spectrum);

    std::size_t N = spectrum.length();

    QVERIFY(N     == result.rowSize);
    QVERIFY(N * N == result.fullSize);

    for (std::size_t i = 0; i < N; i++)
        QVERIFY(fabs(expected[i] - std::accumulate(result.data[i], result.data[i] + N, 0.0)) < 0.0001);
}

QTEST_APPLESS_MAIN(SpectrumTest)

#include "spectrum_test.moc"
