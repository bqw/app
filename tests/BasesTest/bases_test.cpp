#include <iostream>

#include <qmath.h>
#include <QString>
#include <QtTest>

#include "bases.h"


Q_DECLARE_METATYPE(HadamardBasis)
Q_DECLARE_METATYPE(TrigonometricBasis)


class BasesTest : public QObject
{
    Q_OBJECT

public:
    BasesTest();

private:
    static constexpr auto EPSILON = 0.00000001;

private Q_SLOTS:
    void testSin_data();
    void testSin();

    void testCos_data();
    void testCos();

    void testHad_data();
    void testHad();

    void testTrigonometricBasis_data();
    void testTrigonometricBasis();

    void testHadamardBasis_data();
    void testHadamardBasis();
};

BasesTest::BasesTest()
{

}

void BasesTest::testSin_data()
{
    QTest::addColumn<int>("k");
    QTest::addColumn<int>("i");
    QTest::addColumn<int>("N");
    QTest::addColumn<double>("expected");

    QTest::newRow("sin test 0")  << 0 << 1 << 1 << 0.0;
    QTest::newRow("2")           << 1 << 4 << 3 << sin(2 * M_PI * 1.0 * 4.0 / 3.0);
}

void BasesTest::testSin()
{
    QFETCH(int, k);
    QFETCH(int, i);
    QFETCH(int, N);

    QFETCH(double, expected);

    QVERIFY(expected == functions::sin(k, i, N));
}

void BasesTest::testCos_data()
{
    QTest::addColumn<int>("k");
    QTest::addColumn<int>("i");
    QTest::addColumn<int>("N");
    QTest::addColumn<double>("expected");

    QTest::newRow("cos test 0") << 0 << 0 << 1 << 1.0;
}

void BasesTest::testCos()
{
    QFETCH(int, k);
    QFETCH(int, i);
    QFETCH(int, N);

    QFETCH(double, expected);

    QVERIFY(expected == functions::cos(k, i, N));
}

void BasesTest::testHad_data()
{
    QTest::addColumn<int>("k");
    QTest::addColumn<int>("i");
    QTest::addColumn<int>("N");
    QTest::addColumn<double>("expected");

    QTest::newRow("had test") << 0 << 0 << 1 << 1.0;
}

void BasesTest::testHad()
{
    QFETCH(int, k);
    QFETCH(int, i);
    QFETCH(int, N);

    QFETCH(double, expected);

    QVERIFY(expected == functions::had(k, i, N));
}

void BasesTest::testTrigonometricBasis_data()
{
    QTest::addColumn<int>("N");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << 8 << QVector<double>({1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.70710678118654746, 1.0, 0.70710678118654757, 0.0, -0.70710678118654746, -1.0, -0.70710678118654768, 1.0, 0.70710678118654757, 0.0, -0.70710678118654746, -1.0, -0.70710678118654768, 0.0, 0.70710678118654735, 0.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0, -1.0, 1.0, 0.0, -1.0, 0.0, 1.0, 0.0, -1.0, 0.0, 0.0, 0.70710678118654757, -1.0, 0.70710678118654735, 0.0, -0.70710678118654846, 1.0, -0.70710678118654768, 1.0, -0.70710678118654746, 0.0, 0.70710678118654768, -1.0, 0.70710678118654657, 0.0, -0.70710678118654735, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0});
}

void BasesTest::testTrigonometricBasis()
{
    QFETCH(int, N);
    QFETCH(QVector<double>, expected);

    TrigonometricBasis basis(N);

    for (auto k = 0; k < N; k++) {
        QVector<std::size_t> functionNumbers;
        functionNumbers.reserve(N);

        functionNumbers << 0;

        for (auto i = 1; i < N / 2; i++)
            functionNumbers << i << i;

        functionNumbers << N / 2;

        functionNumbers[N - 1] = N / 2;

        for (auto i = 0; i < N; i++)
            QVERIFY(fabs(expected[k * N + i] - basis.at(k)(functionNumbers[k], i)) < BasesTest::EPSILON);
    }
}

void BasesTest::testHadamardBasis_data()
{
    QTest::addColumn<int>("N");
    QTest::addColumn<QVector<double> >("expected");

    QTest::newRow("1") << 8 << QVector<double>({1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, -1, 1, -1, 1, 1, -1, -1, 1, 1, -1, -1, 1, -1, -1, 1, 1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, 1, -1, 1, -1, -1, 1, -1, 1, 1, 1, -1, -1, -1, -1, 1, 1, 1, -1, -1, 1, -1, 1, 1, -1});
}

void BasesTest::testHadamardBasis()
{
    QFETCH(int, N);
    QFETCH(QVector<double>, expected);

    HadamardBasis basis(N);

    for (auto k = 0; k < N; k++)
        for (auto i = 0; i < N; i++)
            QVERIFY(expected[k * N + i] == basis.at(k)(k, i));
}

QTEST_APPLESS_MAIN(BasesTest)

#include "bases_test.moc"
