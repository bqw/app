#include <chrono>
#include <iostream>
#include <memory>

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QChartView>
#include <QtCharts/QChart>

#include <QVector>
#include <QPoint>

#include "algorithm.h"
#include "correlation.h"
#include "density.h"


QT_CHARTS_USE_NAMESPACE

int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);

//    auto algorithm = AlgorithmBuilder::buildNonstationary();

//    AlgorithmParams params {
//            false,
//            100,
//            new WhiteNoiseDensity(M_PI, 5),
//            new MuGeneratorUniform
//    };

//    auto time = std::chrono::system_clock::now();
//    algorithm->init(params);
//    std::cout << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - time).count() << std::endl;

//    auto signal = algorithm->implement();

////    for (auto &elem : signal)
////        std::cout << elem.y() << "  ";

//    std::cout << std::endl;

//    auto series = new QLineSeries();

//    for (auto it = signal.begin(); it != signal.end(); it++)
//        *series << *it;

//    auto correlation = ComputeCorrelationStrategy(
//                new TheoreticCorrelationWhiteNoise(
//                    algorithm->computeSpectrumStrategy()->deltaT(),
//                    algorithm->computeSpectrumStrategy()->density()->cutOffFrequency(),
//                    algorithm->computeSpectrumStrategy()->density()->maximum()
//                    )
//                );

//    QVector<double> range;
//    for (auto i = 0.0; i < algorithm->computeSpectrumStrategy()->N(); i += 0.1)
//        range << i;

//    auto tcorr = correlation(range);

//    QVector<double> sig;

//    for (auto i = 0; i < signal.size(); i++)
//        sig << signal[i].y();

//    correlation.setCorrelation(new ExperimentalCorrelation(sig));

//    auto ecorr = correlation(range);

//    auto tcorrSeries = new QLineSeries();
//    auto ecorrSeries = new QLineSeries();

//    for (auto i = 0; i < range.length(); i++) {
//        *tcorrSeries << QPoint(range[i], tcorr[i]);
//        *ecorrSeries << QPoint(range[i], ecorr[i]);
//    }

//    auto chart = new QChart();

////    chart->addSeries(tcorrSeries);
////    chart->addSeries(ecorrSeries);

//    chart->addSeries(series);
//    chart->legend()->hide();
//    chart->createDefaultAxes();

//    auto view = new QChartView(chart);

//    view->setRenderHint(QPainter::Antialiasing);

//    QMainWindow window;
//    window.setCentralWidget(view);
//    window.resize(640, 480);
//    window.show();

//    return a.exec();
}
