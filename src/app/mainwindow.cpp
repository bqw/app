#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _algorithm(AlgorithmBuilder::buildStationary()),
    _chart(new Chart)
{
    ui->setupUi(this);

    __setValidators();

    ui->densityMaximumLineEdit->setFocus();

    ui->densityTypeComboBox->addItem("Белый шум");
    ui->densityTypeComboBox->addItem("Треугольная");
    ui->densityTypeComboBox->addItem("Экспоненциальная");

    connect(ui->isDeterminateCheckBox, SIGNAL(clicked(bool)), SLOT(changeDeterminate()));

    connect(ui->densityTypeComboBox, SIGNAL(currentIndexChanged(int)),
            this, SLOT(__paramsChanged()));
    connect(ui->isBothSidedCheckBox, SIGNAL(clicked(bool)),
            this, SLOT(__paramsChanged()));
    connect(ui->isDeterminateCheckBox, SIGNAL(clicked(bool)),
            this, SLOT(__paramsChanged()));
    connect(ui->densityMaximumLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(__paramsChanged()));
    connect(ui->cutOffFrequencyLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(__paramsChanged()));
    connect(ui->timeIntervalLineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(__paramsChanged()));

    connect(ui->newExperimentAction,        SIGNAL(triggered(bool)), SLOT(newExperiment()));
    connect(ui->exitAction,                 SIGNAL(triggered(bool)), SLOT(close()));

    connect(ui->setupButton, SIGNAL(clicked(bool)), SLOT(initializeAlgorithm()));
    connect(ui->stationaryImitateButton, SIGNAL(clicked(bool)), SLOT(implementAlgorithm()));

    auto series = new QLineSeries;

    _chart->addSeries(series);
    _chart->setAnimationOptions(QChart::SeriesAnimations);
    _chart->legend()->hide();
    _chart->createDefaultAxes();

    ui->signalView->setChart(_chart);

    ui->signalView->setRenderHint(QPainter::Antialiasing);
    ui->signalView->setRubberBand(QChartView::NoRubberBand);
}

MainWindow::~MainWindow()
{
    delete  ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::__showResults(QVector<QPointF> &points, QTableWidget *signal, QTableWidget *params)
{
    ui->stationarySignalTableWidget->clear();
    ui->nonstationarySignalTableWidget->clear();
    ui->stationaryParamsTableWidget->clear();
    ui->nonstationaryParamsTableWidget->clear();

    signal->setRowCount(points.size());
    signal->setColumnCount(2);

    signal->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    signal->setHorizontalHeaderLabels({"X", "Y"});

    for (auto i = 0; i < points.size(); i++) {
        auto xItem = new QTableWidgetItem(QString::number(points[i].x()));
        auto yItem = new QTableWidgetItem(QString::number(points[i].y()));

        signal->setItem(i, 0, xItem);
        signal->setItem(i, 1, yItem);
    }

    params->setRowCount(3);
    params->setColumnCount(2);

    params->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    auto N      = QString::number(_algorithm->computeSpectrumStrategy()->N());
    auto deltaT = QString::number(_algorithm->computeSpectrumStrategy()->deltaT());
    auto deltaW = QString::number(_algorithm->computeSpectrumStrategy()->deltaW());

    signal->setHorizontalHeaderLabels({"Параметр", "Значение"});

    params->setItem(0, 0, new QTableWidgetItem("Количество точек"));
    params->setItem(0, 1, new QTableWidgetItem(N));

    params->setItem(1, 0, new QTableWidgetItem("Шаг по времени"));
    params->setItem(1, 1, new QTableWidgetItem(deltaT));

    params->setItem(2, 0, new QTableWidgetItem("Шаг по частоте"));
    params->setItem(2, 1, new QTableWidgetItem(deltaW));
}

void MainWindow::__setValidators()
{
    QRegExp rx("[+-]?([0-9]*[.])?[0-9]+");

    ui->timeIntervalLineEdit->setValidator(new QRegExpValidator(rx));
    ui->cutOffFrequencyLineEdit->setValidator(new QRegExpValidator(rx));
    ui->densityMaximumLineEdit->setValidator(new QRegExpValidator(rx));
}

void MainWindow::__showError(const QString &message, const QString &title)
{
    QMessageBox::critical(this, title, message);
}

void MainWindow::__showSuccess(const QString &message, const QString &title)
{
    QMessageBox::information(this, title, message);
}

void MainWindow::changeDeterminate()
{
    _isDeterminate = !_isDeterminate;

    ui->loadCoefficientsGroupBox->setEnabled(_isDeterminate);
}

void MainWindow::implementAlgorithm()
{
    connect(_algorithm.get(), SIGNAL(imitationFinished(QVector<QPointF>)),
            this,             SLOT(implementationFinished(QVector<QPointF>)), Qt::DirectConnection);

    ui->dockWidget_2->setEnabled(false);
    _algorithm->implement();
}

void MainWindow::initializeAlgorithm()
{
    AlgorithmParams params;

    params.isBothSided = ui->isBothSidedCheckBox->isChecked();

    auto ok = false;
    params.timeInterval = ui->timeIntervalLineEdit->text().toDouble(&ok);

    if (!ok) {
        __showError("Неверно задан формат временного интервала");
        return;
    }

    auto cutOffFreq = ui->cutOffFrequencyLineEdit->text().toDouble(&ok);

    if (!ok) {
        __showError("Неверно задана верхняя граничная частота");
        return;
    }

    auto densityMaximum = ui->densityMaximumLineEdit->text().toDouble(&ok);

    if (!ok) {
        __showError("Неверно задано максимальное значение спектральной плотности");
        return;
    }

    AbstractDensity *density = nullptr;

    switch (ui->densityTypeComboBox->currentIndex()) {
    case static_cast<int>(DensityType::WhiteNoise):
        density = new WhiteNoiseDensity(cutOffFreq, densityMaximum);
        break;
    case static_cast<int>(DensityType::Triangular):
        density = new TriangularDensity(cutOffFreq, densityMaximum);
        break;
    case static_cast<int>(DensityType::Exponential):
        density = new ExponentialDensity(cutOffFreq, densityMaximum);
        break;
    default:
        break;
    }

    params.density   = density;
    params.generator = new MuGeneratorUniform;

    _algorithm->setParams(std::move(params));

    connect(_algorithm.get(), SIGNAL(initializationFinished()),
            this,             SLOT(initializationFinished()));

    ui->dockWidget_2->setEnabled(false);
    _algorithm->init();
}

void MainWindow::newExperiment()
{
    ui->densityTypeComboBox->setCurrentIndex(static_cast<int>(DensityType::WhiteNoise));

    ui->timeIntervalLineEdit->text().clear();
    ui->cutOffFrequencyLineEdit->text().clear();

    ui->isBothSidedCheckBox->setChecked(false);
    ui->isDeterminateCheckBox->setChecked(false);

    _chart->removeAllSeries();
}

void MainWindow::exceptionOccured(QString what)
{
    __showError(what);
}

void MainWindow::initializationInterrupted()
{
    ui->dockWidget_2->setEnabled(true);
}

void MainWindow::initializationFinished()
{
    ui->imitationTabWidget->setEnabled(true);
    ui->dockWidget_2->setEnabled(true);
}

void MainWindow::implementationFinished(QVector<QPointF> points)
{
    ui->dockWidget_2->setEnabled(true);

    _signal = points;

    _chart->removeAllSeries();

    auto series = new QLineSeries;
    series->append(_signal.toList());

    _chart->addSeries(series);

    _chart->createDefaultAxes();

    __showResults(points, ui->stationarySignalTableWidget, ui->stationaryParamsTableWidget);
}

void MainWindow::__paramsChanged()
{
    ui->imitationTabWidget->setEnabled(false);
}
