#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCharts/QLineSeries>
#include <QMainWindow>
#include <QMessageBox>
#include <QPoint>
#include <QProgressDialog>
#include <QRegExp>
#include <QTableWidget>
#include <QThread>
#include <QValidator>
#include <QVector>

#include <random>

#include "autocorrelationwindow.h"

#include "chart.h"
#include "chartview.h"

#include "algorithm.h"
#include "density.h"


QT_CHARTS_USE_NAMESPACE


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    enum class DensityType
    {
        WhiteNoise = 0,
        Triangular,
        Exponential
    };

    void __showResults(QVector<QPointF> &points, QTableWidget *signal, QTableWidget *params);

    void __setValidators();

    void __showError(const QString &message, const QString &title = "Ошибка");
    void __showSuccess(const QString &message, const QString &title = "Успех");

    Ui::MainWindow *ui;

    std::unique_ptr<AbstractAlgorithm> _algorithm;

    Chart       *_chart;

    QVector<QPointF> _signal;

    bool _isDeterminate    = false;

public slots:
    void changeDeterminate();

    void implementAlgorithm();
    void initializeAlgorithm();

    void newExperiment();

    void exceptionOccured(QString what);

    void initializationInterrupted();

    void initializationFinished();
    void implementationFinished(QVector<QPointF> points);

private slots:
    void __paramsChanged();
};

#endif // MAINWINDOW_H
