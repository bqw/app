#include "autocorrelationwindow.h"
#include "ui_autocorrelationwindow.h"

AutocorrelationWindow::AutocorrelationWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AutocorrelationWindow)
{
    ui->setupUi(this);
}

AutocorrelationWindow::~AutocorrelationWindow()
{
    delete ui;
}

void AutocorrelationWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
