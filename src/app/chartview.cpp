/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "chartview.h"
#include <QtGui/QMouseEvent>

ChartView::ChartView(QWidget *parent, unsigned int sensivity):
    QChartView(parent),
    _sensivity(sensivity),
    _coordX(0),
    _coordY(0)
{

}

ChartView::ChartView(QChart *chart, unsigned int sensivity, QWidget *parent) :
    QChartView(chart, parent),
    _sensivity(sensivity),
    _coordX(0),
    _coordY(0)
{
//    _coordX = new QGraphicsSimpleTextItem(chart);
//    _coordY = new QGraphicsSimpleTextItem(chart);

//    _coordX->setPos(chart->size().width() - 100, chart->size().height());
//    _coordX->setText("X: ");

//    _coordY->setPos(chart->size().width() - 50, chart->size().height());
//    _coordY->setText("Y: ");
}

ChartView::~ChartView()
{
//    delete _coordX;
//    delete _coordY;
}

bool ChartView::viewportEvent(QEvent *event)
{
    return QChartView::viewportEvent(event);
}

void ChartView::mousePressEvent(QMouseEvent *event)
{
    //chart()->setAnimationOptions(QChart::NoAnimation);
    QChartView::mousePressEvent(event);

    _mousePrevPos = event->globalPos();
}

void ChartView::mouseMoveEvent(QMouseEvent *event)
{
//    _coordX->setText(QString("X: %1").arg(chart()->mapToValue(event->pos()).x()));
//    _coordY->setText(QString("Y: %1").arg(chart()->mapToValue(event->pos()).y()));

    QChartView::mouseMoveEvent(event);
}

void ChartView::mouseReleaseEvent(QMouseEvent *event)
{
    chart()->setAnimationOptions(QChart::SeriesAnimations);

    QChartView::mouseReleaseEvent(event);

    auto delta = event->globalPos() - _mousePrevPos;
    // Изменение знака для естественного "перетаскивания" мышкой, обратного к сматрфонному.
    delta.setX(-delta.x());

    chart()->scroll(delta.x(), delta.y());
}

void ChartView::wheelEvent(QWheelEvent *event)
{
    if (event->angleDelta().y() > 0)
        __zoomIn();
    else
        __zoomOut();
}

void ChartView::keyPressEvent(QKeyEvent *event)
{
    qDebug() << event->count();

    if (Qt::Key_Plus == event->key() && event->modifiers() & Qt::ControlModifier)
        __zoomIn();
    else if (Qt::Key_Minus == event->key() && event->modifiers() & Qt::ControlModifier)
        __zoomOut();
    else if (Qt::Key_0 == event->key() && event->modifiers() & Qt::ControlModifier)
        __zoomReset();

    switch (event->key()) {
    case Qt::Key_Left:
        __scrollLeft();
        break;
    case Qt::Key_Right:
        __scrollRight();
        break;
    case Qt::Key_Up:
        __scrollUp();
        break;
    case Qt::Key_Down:
        __scrollDown();
        break;
    default:
        break;
    }
}
