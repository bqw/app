#ifndef CHARTVIEW_H
#define CHARTVIEW_H

#include <QDebug>

#include <QGraphicsSimpleTextItem>
#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>

QT_CHARTS_USE_NAMESPACE

//![1]
class ChartView : public QChartView
//![1]
{
public:
    ChartView(QWidget *parent = 0,
              unsigned int sensivity = ChartView::DEFAULT_SENSIVITY);
    ChartView(QChart *chart,
              unsigned int sensivity = ChartView::DEFAULT_SENSIVITY,
              QWidget *parent        = 0);

    ~ChartView();

//![2]
protected:
    bool viewportEvent(QEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);
//![2]

private:
    inline void __zoomIn()
    {
        chart()->zoomIn();
    }

    inline void __zoomOut()
    {
        chart()->zoomOut();
    }

    inline void __zoomReset()
    {
        chart()->zoomReset();
    }

    inline void __scrollLeft()
    {
        chart()->scroll(-_sensivity, 0);
    }

    inline void __scrollRight()
    {
        chart()->scroll(_sensivity, 0);
    }

    inline void __scrollUp()
    {
        chart()->scroll(0, _sensivity);
    }

    inline void __scrollDown()
    {
        chart()->scroll(0, -_sensivity);
    }

    static auto constexpr DEFAULT_SENSIVITY = 25;

    int _sensivity;

    QGraphicsSimpleTextItem *_coordX;
    QGraphicsSimpleTextItem *_coordY;

    QPoint _mousePrevPos;
};

#endif
