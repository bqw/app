#ifndef AUTOCORRELATIONWINDOW_H
#define AUTOCORRELATIONWINDOW_H

#include <QWidget>

namespace Ui {
class AutocorrelationWindow;
}

class AutocorrelationWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AutocorrelationWindow(QWidget *parent = 0);
    ~AutocorrelationWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::AutocorrelationWindow *ui;
};

#endif // AUTOCORRELATIONWINDOW_H
