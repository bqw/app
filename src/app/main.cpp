#include "mainwindow.h"
#include <QApplication>

#include "autocorrelationwindow.h"

int main(int argc, char *argv[])
{
    qRegisterMetaType<QString>("QString");
    qRegisterMetaType<QVector<QPointF> >("QVector<QPointF>");

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
