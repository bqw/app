#include "bases.h"


Basis::~Basis()
{

}

const BasicFunction &Basis::at(std::size_t i)
{
    if (i >= _N) {
        qDebug() << i << "\n";
        throw ImitationException("Базисной функции с таким номером не существует");
    }

    return this->operator [](i);
}

Basis &Basis::setN(std::size_t N)
{
    _N = N;

    return *this;
}


const BasicFunction &SingleBasis::operator [](std::size_t)
{
    return _f;
}

SingleBasis &SingleBasis::setN(std::size_t N)
{
    Basis::setN(N);

    _f.setN(N);

    return *this;
}

SingleBasis &SingleBasis::setFunction(double (*f)(int, int, std::size_t))
{
    _f.setFunction(f);

    return *this;
}


HadamardBasis &HadamardBasis::setN(std::size_t N)
{
    // Проверка на степень 2.
    if ((N & (N - 1)) != 0)
        throw ImitationException("Количество точек должно быть двоично-рациональным");

    SingleBasis::setN(N);

    return *this;
}


const BasicFunction &PairBasis::operator [](std::size_t i)
{
    if (0 == i || _N - 1 == i)
        return _fe;

    return (0 == i % 2) ? _fe : _fo;
}

PairBasis &PairBasis::setN(std::size_t N)
{
    Basis::setN(N);

    _fe.setN(N);
    _fo.setN(N);

    return *this;
}

PairBasis &PairBasis::setFunctionEven(double (*f)(int, int, std::size_t))
{
    _fe.setFunction(f);

    return *this;
}

PairBasis &PairBasis::setFunctionOdd(double (*f)(int, int, std::size_t))
{
    _fo.setFunction(f);

    return *this;
}
