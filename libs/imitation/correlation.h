#ifndef CORRELATION_H
#define CORRELATION_H

#include <QVector>

#include <algorithm>

#include "density.h"
#include "imitationexception.h"


class AbstractCorrelation;


/**
 * @brief Стратегия вычисления корреляционной функции.
 */
class ComputeCorrelationStrategy
{
public:
    /**
     * @brief Конструктор.
     * @param correlation Указатель на корреляционную функцию.
     */
    explicit inline ComputeCorrelationStrategy(AbstractCorrelation *correlation = nullptr):
        _correlation(correlation)
    {

    }

    /**
      * @brief Деструктор.
      * @return
      */
    ~ComputeCorrelationStrategy();

    /**
     * @brief Операция вычисления массива значений корреляционной функции.
     * @param range Массив значений исходного сигнала.
     * @return
     */
    QVector<double> operator ()(const QVector<double> &range);

    /**
     * @brief Вызывает операцию вычисления массива значений корреляционной функции.
     * Введено для более удобной работы с объектом через указатель.
     * @param range Массив значений исходного сигнала.
     * @return
     */
    inline QVector<double> call(const QVector<double> &range)
    {
        return this->operator ()(range);
    }

    /**
     * @brief Сеттер корреляционной функции.
     * @param correlation Указатель на корреляционную функцию.
     * @return
     */
    ComputeCorrelationStrategy &setCorrelation(AbstractCorrelation *correlation);

private:
    /**
     * @brief Указатель на корреляционную функцию.
     */
    AbstractCorrelation *_correlation;
};


/**
 * @brief Абстрактный класс дискретных корреляционных функций.
 */
class AbstractCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param deltaT Шаг по времени.
     */
    explicit inline AbstractCorrelation(double deltaT = 0.0):
        _deltaT(deltaT)
    {

    }

    /**
     * @brief Деструктор.
     */
    virtual ~AbstractCorrelation();

    /**
     * @brief Операция вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    virtual double operator ()(int i) = 0;

    /**
     * @brief Вызывает операцию вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    inline double call(int i)
    {
        return this->operator ()(i);
    }

    /**
     * @brief Сеттер шага по времени.
     * @param deltaT Шаг по времени.
     * @return
     */
    AbstractCorrelation &setDeltaT(double deltaT);

    /**
     * @brief Геттер шага по времени.
     * @return
     */
    inline double deltaT() const
    {
        return _deltaT;
    }

protected:
    /**
     * @brief Шаг по времени.
     */
    double _deltaT;
};


/**
 * @brief Абстрактный класс теоретических корреляционных функций.
 * @note  Не явялется классом, управляющим ресурсами, поэтому указатель на ФСПМ не освобождается.
 */
class AbstractTheoreticCorrelation : public AbstractCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param deltaT          Шаг по времени.
     * @param cutOffFrequency Верхняя граничная циклическая частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline AbstractTheoreticCorrelation(double deltaT          = 0.0,
                                                 double cutOffFrequency = 0.0,
                                                 double densityMaximum  = 0.0):
        AbstractCorrelation(deltaT),
        _w_h(cutOffFrequency),
        _S(densityMaximum)
    {

    }

    /**
     * @brief Конструктор.
     * @param deltaT  Шаг по времени.
     * @param density Указатель на ФСПМ с необходимыми характеристиками.
     */
    explicit inline AbstractTheoreticCorrelation(double deltaT, AbstractDensity *density):
        AbstractCorrelation(deltaT),
        _w_h(nullptr == density ? 0.0 : density->cutOffFrequency()),
        _S(nullptr == density ? 0.0 : density->maximum())
    {
        if (nullptr == density)
            throw ImitationException("Указатель на ФСПМ должен быть ненулевым.");
    }

    /**
     * @brief Установка параметров корреляционной функции из ФСПМ.
     * @param density Указатель на ФСПМ.
     * @return
     */
    AbstractTheoreticCorrelation &fromDensity(AbstractDensity *density);

    /**
     * @brief Сеттер верхней граничной частоты.
     * @param cutOffFrequency Верхняя граничная частота.
     * @return
     */
    AbstractTheoreticCorrelation &setCutOffFrequency(double cutOffFrequency);
    /**
     * @brief Сеттер максимального значения ФСПМ.
     * @param densityMaximum Максимальное значение ФСПМ.
     * @return
     */
    AbstractTheoreticCorrelation &setDensityMaximum(double densityMaximum);

    /**
     * @brief Геттер верхней граничной частоты.
     * @return
     */
    inline double cutOffFrequency() const
    {
        return _w_h;
    }

    /**
     * @brief Геттер максимального значения ФСПМ
     * @return
     */
    inline double densityMaximum() const
    {
        return _S;
    }

protected:
    /**
     * @brief Верхняя граничная частота.
     */
    double _w_h;
    /**
     * @brief Максимальное значение ФСПМ.
     */
    double _S;
};


/**
 * @brief Теоретическая корреляция физического белого шума.
 */
class TheoreticCorrelationWhiteNoise : public AbstractTheoreticCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param deltaT          Шаг по времени.
     * @param cutOffFrequency Верхняя граничная циклическая частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline TheoreticCorrelationWhiteNoise(double deltaT          = 0.0,
                                                   double cutOffFrequency = 0.0,
                                                   double densityMaximum  = 0.0):
        AbstractTheoreticCorrelation(deltaT, cutOffFrequency, densityMaximum)
    {

    }

    /**
     * @brief Конструктор.
     * @param deltaT  Шаг по времени.
     * @param density Указатель на ФСПМ с необходимыми характеристиками.
     */
    explicit inline TheoreticCorrelationWhiteNoise(double deltaT, AbstractDensity *density):
        AbstractTheoreticCorrelation(deltaT, density)
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    double operator ()(int i) override;
};


/**
 * @brief Теоретическая корреляция сигнала с треугольной ФСПМ.
 */
class TheoreticCorrelationTriangular : public AbstractTheoreticCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param deltaT          Шаг по времени.
     * @param cutOffFrequency Верхняя граничная циклическая частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline TheoreticCorrelationTriangular(double deltaT          = 0.0,
                                                   double cutOffFrequency = 0.0,
                                                   double densityMaximum  = 0.0):
        AbstractTheoreticCorrelation(deltaT, cutOffFrequency, densityMaximum)
    {

    }

    /**
     * @brief Конструктор.
     * @param deltaT  Шаг по времени.
     * @param density Указатель на ФСПМ с необходимыми характеристиками.
     */
    explicit inline TheoreticCorrelationTriangular(double deltaT, AbstractDensity *density):
        AbstractTheoreticCorrelation(deltaT, density)
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    double operator ()(int i) override;
};


/**
 * @brief Теоретическая корреляция сигнала с экспоненциальной ФСПМ.
 */
class TheoreticCorrelationExponential : public AbstractTheoreticCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param deltaT          Шаг по времени.
     * @param cutOffFrequency Верхняя граничная циклическая частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline TheoreticCorrelationExponential(double deltaT          = 0.0,
                                                   double cutOffFrequency = 0.0,
                                                   double densityMaximum  = 0.0):
        AbstractTheoreticCorrelation(deltaT, cutOffFrequency, densityMaximum)
    {

    }

    /**
     * @brief Конструктор.
     * @param deltaT  Шаг по времени.
     * @param density Указатель на ФСПМ с необходимыми характеристиками.
     */
    explicit inline TheoreticCorrelationExponential(double deltaT, AbstractDensity *density):
        AbstractTheoreticCorrelation(deltaT, density)
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    double operator ()(int i) override;
};


/**
 * @brief Экспериментальная корреляция.
 */
class ExperimentalCorrelation : public AbstractCorrelation
{
public:
    /**
     * @brief Конструктор.
     * @param signal Сигнал для вычисления АКФ.
     */
    explicit inline ExperimentalCorrelation(const QVector<double> &signal = {}):
        _signal(signal)
    {
        __computeCharacteristics();
    }

    /**
     * @brief Перегруженная операция вычисления значения корреляционной функции i-го отсчета.
     * @param i Номер отсчета.
     * @return
     */
    double operator ()(int i) override;

    /**
     * @brief Сеттер сигнала.
     * @param signal
     * @return
     */
    ExperimentalCorrelation &setSignal(const QVector<double> &signal);

    /**
     * @brief Геттер сигнала.
     * @return
     */
    inline const QVector<double> &signal() const
    {
        return _signal;
    }

    /**
     * @brief Геттер математического ожидания сигнала.
     * @return
     */
    inline double mean() const
    {
        return _mean;
    }

    /**
     * @brief Геттер дисперсии сигнала.
     * @return
     */
    inline double std() const
    {
        return _std;
    }

protected:
    /**
     * @brief Вспомогательная функция для расчета статистических характеристик сигнала.
     */
    inline void __computeCharacteristics()
    {
        _mean = 0.0;
        _std  = 0.0;

        if (_signal.isEmpty())
            return;

        _mean = std::accumulate(_signal.begin(), _signal.end(), 0.0) / double(_signal.size());
        _std  = std::accumulate(_signal.begin(), _signal.end(), 0.0,
                                [this](double elem, int){ return pow(elem, 2) - _mean; });
    }

    /**
     * @brief Сигнал.
     */
    QVector<double> _signal;

    /**
     * @brief Математическое ожидание сигнала.
     */
    double _mean;
    /**
     * @brief Дисперсия сигнала.
     */
    double _std;
};

#endif // CORRELATION_H
