#include "imitationexception.h"

ImitationException::ImitationException(const char *message):
    QException(),
    _message(message)
{

}

void ImitationException::raise() const
{
    throw *this;
}

ImitationException *ImitationException::clone() const
{
    return new ImitationException(*this);
}

const char *ImitationException::what() const noexcept
{
    return _message;
}


WrongDensityType *WrongDensityType::clone() const
{
    return new WrongDensityType(*this);
}


WrongLambdaSize *WrongLambdaSize::clone() const
{
    return new WrongLambdaSize(*this);
}
