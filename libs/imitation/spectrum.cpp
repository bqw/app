#include "spectrum.h"


ComputeSpectrumStrategy::~ComputeSpectrumStrategy()
{
    if (nullptr != _S)
        delete _S;
}

QVector<double> ComputeSpectrumStrategy::operator ()(double wMaximum)
{
    if (wMaximum > _S->cutOffFrequency())
        throw ImitationException("Задаваемая частота среза не может быть больше частоты среза ФСПМ");

    if (_l.isEmpty())
        _l.fill(1, _N / 2);

    if (_l.length() != static_cast<int>(_N / 2))
        throw WrongLambdaSize();

    QVector<double> spectrum;
    spectrum.reserve(_N);

    std::cout << _N << std::endl;

    spectrum << sqrt(_S->call(0) / (2 * _T));

    for (std::size_t k = 1; k < static_cast<std::size_t>(floor(_N / 2)); k++) {
        auto even = sqrt(_S->call(2 * M_PI * k / _T) / _T);

        spectrum << even << even;
    }

    spectrum << sqrt(_S->call(M_PI / _deltaT) / (2 * _T));

    return spectrum;
}

ComputeSpectrumStrategy &ComputeSpectrumStrategy::clearLambdas()
{
    _l.clear();

    return *this;
}

ComputeSpectrumStrategy &ComputeSpectrumStrategy::setTimeInterval(double timeInterval)
{
    if (0.0 >= timeInterval)
        throw ImitationException("Временной интервал должен быть неотрицательным");

    _T = timeInterval;

    _deltaW = __computeDeltaW();
    _N      = __computeN();

    _deltaT = _T / _N;

    return *this;
}

ComputeSpectrumStrategy &ComputeSpectrumStrategy::setDensity(AbstractDensity *density)
{
    __checkDensity(density);

    if (nullptr != _S)
        delete _S;

    _S = density;

    _deltaT = __computeDeltaT();
    _N      = __computeN();

    _deltaT = _T / _N;

    return *this;
}

ComputeSpectrumStrategy &ComputeSpectrumStrategy::setLambdas(const QVector<double> &lambdas)
{
    _l = lambdas;

    return *this;
}

std::size_t ComputeSpectrumStrategy::__computeN()
{
    return (0 == _deltaT) ? 0.0 : _T / _deltaT;
}


std::size_t ComputeSpectrumStrategyHadamard::__computeN()
{
    return pow(2, ceil(log2(_T / _deltaT)));
}


TransformSpectrumStrategy::~TransformSpectrumStrategy()
{
    __deleteKernel();

    if (nullptr != _currentBasis)
        delete _currentBasis;

    if (nullptr != _targetBasis)
        delete _targetBasis;
}

TransformSpectrumStrategy::TransformedSpectrum TransformSpectrumStrategy::operator ()(const QVector<double> &spectrum)
{
    if (nullptr != _currentBasis && nullptr != _targetBasis && _currentBasis->N() != _targetBasis->N())
        throw ImitationException("Базисы не могут быть преобразованы друг в друга - разное количество точек разбиения");

    if (nullptr == _kernel || spectrum.isEmpty())
        return {0, 0, nullptr};

    if (_changed)
        init();

//    for (std::size_t i = 0; i < _N; i++) {
//        for (std::size_t j = 0; j < _N; j++)
//            std::cout << std::left << std::setw(10) << _kernel[i][j] << " ";

//        std::cout << std::endl;
//    }

    std::cout << std::endl;

//    for (auto &el : spectrum)
//        std::cout << std::setw(6) << el << " ";

//    std::cout << std::endl << std::endl;

    double **result = __allocate();

//    for (auto i = 0; i < _N; i++) {
//        for (auto j = 0; j < _N; j++)
//            std::cout << std::setw(6) << result[i][j] << " ";

//        std::cout << std::endl;
//    }

    for (std::size_t i = 0; i < _N; i++) {
        for (std::size_t j = 0; j < _N; j++) {
            auto tmp     = spectrum[j] * _kernel[i][j];
            result[i][j] = (fabs(tmp) > 10e-10) ? tmp : 0.0;
        }
    }

//    for (auto i = 0; i < _N; i++) {
//        for (auto j = 0; j < _N; j++)
//            std::cout << std::setw(6) << result[i][j] << " ";

//        std::cout << std::endl;
//    }

    return {_N, _N * _N, result};
}

TransformSpectrumStrategy &TransformSpectrumStrategy::init()
{
    if (_changed && nullptr != _kernel) {
        __computeKernel();
        _changed = false;
    }

    return *this;
}

TransformSpectrumStrategy &TransformSpectrumStrategy::setN(std::size_t N)
{
    if (_N == N)
        return *this;

    __deleteKernel();

    _N       = N;
    _changed = true;

    _kernel = __allocate();

    if (nullptr != _currentBasis)
        _currentBasis->setN(N);

    if (nullptr != _targetBasis)
        _targetBasis->setN(N);

    return *this;
}

TransformSpectrumStrategy &TransformSpectrumStrategy::setCurrentBasis(Basis *currentBasis)
{
    if (nullptr != _currentBasis)
        delete _currentBasis;

    _currentBasis = currentBasis;
    _changed      = true;

    _currentBasis->setN(_N);

    return *this;
}

TransformSpectrumStrategy &TransformSpectrumStrategy::setTargetBasis(Basis *targetBasis)
{
    if (nullptr != _targetBasis)
        delete _targetBasis;

    _targetBasis = targetBasis;
    _changed     = true;

    _targetBasis->setN(_N);

    return *this;
}
