#ifndef FACTORY_H
#define FACTORY_H

#include <QSharedPointer>

#include "bases.h"
#include "correlation.h"
#include "density.h"
#include "fourier.h"

#endif // FACTORY_H
