#include "generator.h"


QVector<double> AbstractLambdaGenerator::operator ()(std::size_t length)
{
    QVector<double> result(length);

    for (std::size_t i = 0; i < length; i++)
        result[i] = AbstractGenerator::get();

    return result;
}


double LambdaGeneratorNormal::operator ()()
{
    return _dist(_dev);
}


double MuGeneratorUniform::operator ()()
{
    return (_dist(_dev) >= MuGeneratorUniform::BORDER) ? 1 : -1;
}

QVector<QPair<double, double> > MuGeneratorUniform::operator ()(std::size_t length)
{
    if (length > static_cast<std::size_t>(std::numeric_limits<int>::max()))
        throw ImitationException("Слишком большая длина");

    QVector<QPair<double, double> > result;
    result.reserve(static_cast<int>(length));

    for (std::size_t i = 0; i < length; i++)
        result << QPair<double, double>(get(), get());

    return result;
}
