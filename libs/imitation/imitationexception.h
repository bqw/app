#ifndef IMITATIONEXCEPTION_H
#define IMITATIONEXCEPTION_H

#include <QException>


/**
 * @brief Базовый класс исключений библиотеки.
 */
class ImitationException : public QException
{
public:
    ImitationException(const char *message = "");

    void raise() const;
    ImitationException *clone() const;

    const char *what() const noexcept;

protected:
    /**
     * @brief Сообщение об ошибке.
     */
    const char *_message;
};


/**
 * @brief Неверный класс ФСПМ.
 */
class WrongDensityType : public ImitationException
{
public:
    WrongDensityType *clone() const;

protected:
    const char *_message = "Задан неверный класс ФСПМ сигнала";
};


/**
 * @brief Неверное количество параметров сигнала.
 */
class WrongLambdaSize : public ImitationException
{
public:
    WrongLambdaSize *clone() const;

protected:
    const char *_message = "Invalig coefficients size";
};


#endif // IMITATIONEXCEPTION_H
