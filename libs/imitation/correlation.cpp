#include "correlation.h"

ComputeCorrelationStrategy::~ComputeCorrelationStrategy()
{
    if (nullptr != _correlation)
        delete _correlation;
}

QVector<double> ComputeCorrelationStrategy::operator ()(const QVector<double> &range)
{
    if (range.isEmpty())
        return {};

    QVector<double> result;

    result.reserve(range.size());

    for (auto i : range)
        result << _correlation->call(i);

    return result;
}

ComputeCorrelationStrategy &ComputeCorrelationStrategy::setCorrelation(AbstractCorrelation *correlation)
{
    if (nullptr != _correlation)
        delete _correlation;

    _correlation = correlation;

    return *this;
}


AbstractCorrelation::~AbstractCorrelation()
{

}

AbstractCorrelation &AbstractCorrelation::setDeltaT(double deltaT)
{
    _deltaT = deltaT;

    return *this;
}


AbstractTheoreticCorrelation &AbstractTheoreticCorrelation::fromDensity(AbstractDensity *density)
{
    if (nullptr == density)
        return *this;

    _w_h = density->cutOffFrequency();
    _S   = density->maximum();

    return *this;
}

AbstractTheoreticCorrelation &AbstractTheoreticCorrelation::setCutOffFrequency(double cutOffFrequency)
{
    _w_h = cutOffFrequency;

    return *this;
}

AbstractTheoreticCorrelation &AbstractTheoreticCorrelation::setDensityMaximum(double densityMaximum)
{
    _S = densityMaximum;

    return *this;
}


double TheoreticCorrelationWhiteNoise::operator ()(int i)
{
    auto x = i * _deltaT * _w_h;

    return (0 != x) ? (_S * _w_h / M_PI) * (sin(x) / x) :  _S * _w_h / M_PI;
}


double TheoreticCorrelationTriangular::operator ()(int i)
{
    auto x = i * _deltaT * _w_h;

    return (0 != x) ? (1 - cos(x)) * _S * (M_PI * _w_h * pow(i * _deltaT, 2)) : _S * _w_h /  (2 * M_PI);
}


double TheoreticCorrelationExponential::operator ()(int i)
{
    auto x = i * _deltaT * _w_h;

    return (0 != x) ? _S * exp(- fabs(x)) : _S;
}


double ExperimentalCorrelation::operator ()(int i)
{
    auto sum = 0.0;

    for (auto k = 0; k < _signal.size() - i; k++)
        sum += (_signal[i] - _mean) * (_signal[i + k] - _mean);

    return 1 / (_std * _signal.size()) * sum;
}

ExperimentalCorrelation &ExperimentalCorrelation::setSignal(const QVector<double> &signal)
{
    _signal = signal;

    __computeCharacteristics();

    return *this;
}
