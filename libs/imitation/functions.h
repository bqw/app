#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cstddef>

#include <qmath.h>


/**
 * Все используемые базисные функции.
 */
namespace functions
{

/**
 * @brief sin Нечетная составляющая тригонометрического базиса.
 * @param k
 * @param i
 * @param N   Количество отсчетов дискретного спектра.
 * @return
 */
double sin(int k, int i, std::size_t N);

/**
 * @brief cos Четная составляющая тригонометрического базиса.
 * @param k
 * @param i
 * @param N   Количество отсчетов дискретного спектра.
 * @return
 */
double cos(int k, int i, std::size_t N);

/**
 * @brief had Базисная функция Адамара.
 * @param k
 * @param i
 * @param N   ln2 количества отсчетов дискретного спектра.
 * @return
 */
double had(int k, int i, std::size_t n);

}


class BasicFunction
{
public:
    inline BasicFunction()
    {

    }

    explicit inline BasicFunction(std::size_t N, double (* f)(int, int, std::size_t)):
        _N(N),
        _f(f)
    {

    }

    BasicFunction(const BasicFunction &rhs);

    const BasicFunction &operator =(const BasicFunction &rhs);

    inline double operator ()(int k, int i) const
    {
        return _f(k, i, _N);
    }

    inline double call(int k, int i) const
    {
        return this->operator ()(k, i);
    }

    BasicFunction &setN(std::size_t N);
    BasicFunction &setFunction(double (* f)(int, int, std::size_t));

private:
    std::size_t _N;
    double (* _f)(int, int, std::size_t);
};

#endif // FUNCTIONS_H
