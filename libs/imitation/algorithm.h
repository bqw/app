#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <iostream>
#include <memory>

#include <QObject>
#include <QPoint>
#include <QVector>

#include "bases.h"
#include "density.h"
#include "fourier.h"
#include "generator.h"
#include "spectrum.h"


struct AlgorithmParams
{
    bool                 isBothSided;
    double               timeInterval;
    AbstractDensity     *density;
    AbstractMuGenerator *generator;
    QVector<double>      lambdas;

    AlgorithmParams()
    {

    }

    AlgorithmParams(bool isBothSided_, double timeInterval_,
                    AbstractDensity *density_, AbstractMuGenerator *generator_ = nullptr):
        isBothSided(isBothSided_),
        timeInterval(timeInterval_),
        density(density_),
        generator(generator_)
    {

    }

    AlgorithmParams &operator =(AlgorithmParams &&rhs)
    {
        isBothSided  = rhs.isBothSided;
        timeInterval = rhs.timeInterval;
        density      = rhs.density;
        generator    = rhs.generator;
        lambdas      = std::move(rhs.lambdas);

        rhs.density   = nullptr;
        rhs.generator = nullptr;

        return *this;
    }
};

// TODO: параметры алгоритма задавать через сеттеры
// TODO: фасад

class AlgorithmBuilder;

class AbstractAlgorithm : public QObject
{
    Q_OBJECT

public:
    explicit AbstractAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy   = nullptr,
                               TransformSpectrumStrategy    *transformSpectrumStrategy = nullptr,
                               AbstractBackFourierTransform *fourierTransform          = nullptr);

    ~AbstractAlgorithm();

    AbstractAlgorithm &setComputeSpectrumStrategy(ComputeSpectrumStrategy *computeSpectrumStrategy);
    AbstractAlgorithm &setTransfromSpectrumStrategy(TransformSpectrumStrategy *transformSpectrumStrategy);
    AbstractAlgorithm &setIsBothSided(bool isBothSided);

    virtual AbstractAlgorithm &setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform) = 0;

    AbstractAlgorithm &setParams(AlgorithmParams &&params);

    const ComputeSpectrumStrategy *computeSpectrumStrategy() const
    {
        return _computeSpec;
    }

    const TransformSpectrumStrategy *transformSpectrumStrategy() const
    {
        return _transformSpec;
    }

    const AbstractBackFourierTransform *fourierTransformStrategy() const
    {
        return _computeSignal;
    }

protected:
    inline void __checkInitialized() const
    {
        if (nullptr == _computeSpec || nullptr == _computeSignal || nullptr == _transformSpec)
            throw ImitationException("Алгоритм не инициализирован");
    }

    inline QVector<double> __prepareSpectrum()
    {
        __checkInitialized();

        if (_needsInitialize)
            throw ImitationException("Необходимо установить параметры алгоритма методом init()");

        return _computeSpec->call();
    }

    inline QVector<QPointF> __toPoints(const QVector<double> &signal) const
    {
        QVector<QPointF> result;
        result.reserve(signal.size());

        auto x      = 0.0;
        auto deltaT = _computeSpec->timeInterval() / _computeSpec->N();

        for (auto i = 0; i < signal.size(); i++) {
            result << QPointF(x, signal[i]);
            x += deltaT;
        }

        return result;
    }

    ComputeSpectrumStrategy      *_computeSpec   = nullptr;
    TransformSpectrumStrategy    *_transformSpec = nullptr;
    AbstractBackFourierTransform *_computeSignal = nullptr;

    bool _needsInitialize;

    bool _isBothSided;

    AbstractMuGenerator *_generatorBuffer;

private:
    AlgorithmParams _params;

signals:
    void errorOccured(QString what);
    void initializationFinished();
    void imitationFinished();
    void imitationFinished(QVector<QPointF> signal);

public slots:
    void                    init();
    virtual QVector<QPointF> implement() = 0;
};


class StationaryAlgorithm : public AbstractAlgorithm
{
    Q_OBJECT

public:
    StationaryAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy   = nullptr,
                        TransformSpectrumStrategy    *transformSpectrumStrategy = nullptr,
                        AbstractBackFourierTransform *fourierTransform          = nullptr);

    StationaryAlgorithm &setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform) override;

public slots:
    QVector<QPointF> implement() override;
};


class NonstationaryAlgorithm : public AbstractAlgorithm
{
    Q_OBJECT

public:
    NonstationaryAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy   = nullptr,
                           TransformSpectrumStrategy    *transformSpectrumStrategy = nullptr,
                           AbstractBackFourierTransform *fourierTransform          = nullptr);

    NonstationaryAlgorithm &setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform) override;

public slots:
    QVector<QPointF> implement() override;
};


class AlgorithmBuilder
{
public:
    static inline std::unique_ptr<StationaryAlgorithm> buildStationary()
    {
        auto algorithm = std::unique_ptr<StationaryAlgorithm>(new StationaryAlgorithm);

        algorithm->setComputeSpectrumStrategy(new ComputeSpectrumStrategyHadamard);
        algorithm->setTransfromSpectrumStrategy(new TransformSpectrumStrategy(new TrigonometricBasis, new HadamardBasis));
        algorithm->setFourierTransformStrategy(new StationaryBackHadamardTransfrom);

        return algorithm;
    }

    static inline std::unique_ptr<NonstationaryAlgorithm> buildNonstationary()
    {
        auto algorithm = std::unique_ptr<NonstationaryAlgorithm>(new NonstationaryAlgorithm);

        algorithm->setComputeSpectrumStrategy(new ComputeSpectrumStrategyHadamard);
        algorithm->setTransfromSpectrumStrategy(new TransformSpectrumStrategy(new TrigonometricBasis, new HadamardBasis));
        algorithm->setFourierTransformStrategy(new NonstationaryBackHadamardTransform);

        return algorithm;
    }
};

//class AlgorithmBuilder
//{
//public:
//    static Algorithm *createDefault();
//    static Algorithm *createEmpty();
//};

#endif // ALGORITHM_H
