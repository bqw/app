#include "functions.h"


double functions::sin(int k, int i, std::size_t N)
{
    auto res = ::sin(2 * M_PI * static_cast<double>(k) * static_cast<double>(i) / static_cast<double>(N));
    return res;
}

double functions::cos(int k, int i, std::size_t N)
{
    return ::cos(2 * M_PI * static_cast<double>(k) * static_cast<double>(i) / static_cast<double>(N));
}

double functions::had(int k, int i, std::size_t n)
{
    if (0 == n)
        return 1.0;

    auto value      = k & i;
    auto bitCounter = 0;

    for (bitCounter = 0; value; bitCounter++)
        value &= value - 1;

    return ::cos(M_PI * bitCounter);
}


BasicFunction::BasicFunction(const BasicFunction &rhs):
    _N(rhs._N),
    _f(rhs._f)
{

}

const BasicFunction &BasicFunction::operator =(const BasicFunction &rhs)
{
    _N = rhs._N;
    _f = rhs._f;

    return *this;
}

BasicFunction &BasicFunction::setN(std::size_t N)
{
    _N = N;

    return *this;
}

BasicFunction &BasicFunction::setFunction(double (*f)(int, int, std::size_t))
{
    _f = f;

    return *this;
}
