#include "density.h"

AbstractDensity::~AbstractDensity()
{

}

AbstractDensity &AbstractDensity::setCutOffFrequency(double cutOffFrequency)
{
    _w_h = fabs(cutOffFrequency);

    return *this;
}

AbstractDensity &AbstractDensity::setMaximum(double densityMaximum)
{
    _S = fabs(densityMaximum);

    return *this;
}


double WhiteNoiseDensity::operator ()(double w)
{
    return (fabs(w) <= _w_h) ? _S : 0;
}


double TriangularDensity::operator ()(double w)
{
    return (fabs(w) <= _w_h) ? (_S - _S * w / _w_h) : 0;
}


double ExponentialDensity::operator ()(double w)
{
    return 2 * _S * _w_h / (_w_h_2 + pow(w, 2));
}

ExponentialDensity &ExponentialDensity::setCutOffFrequency(double cutOffFrequency)
{
    _w_h   = cutOffFrequency;
    _w_h_2 = pow(_w_h, 2);

    return *this;
}
