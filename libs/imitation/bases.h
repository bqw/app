#ifndef BASES_H
#define BASES_H

#include <QDebug>
#include <QException>

#include "functions.h"
#include "imitationexception.h"


class TransformSpectrumStrategy;


/**
 * @brief Базовый класс базисов.
 */
class Basis
{
public:
    virtual ~Basis();

    void operator =(const Basis &rhs);

    virtual const BasicFunction &operator [](std::size_t i) = 0;

    virtual const BasicFunction &at(std::size_t i);

    virtual Basis &setN(std::size_t N);

    inline std::size_t N() const
    {
        return _N;
    }

protected:
    explicit inline Basis(std::size_t N = 0):
        _N(N)
    {

    }

    std::size_t _N;
};


/**
 * @brief Базовый класс базисов, состоящих из одной базисной функции.
 */
class SingleBasis : public Basis
{
public:
    void operator =(const SingleBasis &rhs);

    const BasicFunction &operator [](std::size_t) override;

    SingleBasis &setN(std::size_t N) override;
    SingleBasis &setFunction(double (* f)(int, int, std::size_t));

protected:
    inline SingleBasis():
        Basis()
    {

    }

    explicit inline SingleBasis(std::size_t N, double (* f)(int, int, std::size_t)):
        Basis(N)
    {
        _f.setN(N);
        _f.setFunction(f);
    }

    /**
     * Указатель на базисную функцию.
     */
    BasicFunction _f;
};


class HadamardBasis : public SingleBasis
{
public:
    explicit inline HadamardBasis(std::size_t N = 0):
        SingleBasis(N, &functions::had)
    {

    }

    HadamardBasis &setN(std::size_t N) override;
};


/**
 * @brief Базовый класс базисов, состоящих из четной и нечетной базисных функций.
 */
class PairBasis : public Basis
{
public:
    void operator =(const PairBasis &rhs);

    const BasicFunction &operator [](std::size_t i) override;

    PairBasis &setN(std::size_t N) override;
    PairBasis &setFunctionEven(double (* f)(int, int, std::size_t));
    PairBasis &setFunctionOdd(double (* f)(int, int, std::size_t));

protected:
    inline PairBasis():
        Basis()
    {

    }

    explicit inline PairBasis(std::size_t N,
                              double (* fe)(int, int, std::size_t),
                              double (* fo)(int, int, std::size_t)):
        Basis(N),
        _fe(N, fe),
        _fo(N, fo)
    {

    }

    /**
     * Четная базисная функция.
     */
    BasicFunction _fe;
    /**
     * Нечетная базисная функция.
     */
    BasicFunction _fo;
};


class TrigonometricBasis : public PairBasis
{
public:
    explicit inline TrigonometricBasis(std::size_t N = 0):
        PairBasis(N, functions::cos, &functions::sin)
    {

    }
};

#endif // BASES_H
