#ifndef FOURIER_H
#define FOURIER_H

#include <iomanip>

#include <QVector>

#include "bases.h"
#include "generator.h"
#include "imitationexception.h"
#include "spectrum.h"


// TODO: Сеттеры базисов.

class AbstractBackFourierTransform
{
public:
    explicit inline AbstractBackFourierTransform(Basis               *basis     = nullptr,
                                                 AbstractMuGenerator *generator = nullptr):
        _basis(basis),
        _g(generator)
    {

    }

    virtual ~AbstractBackFourierTransform()
    {
        if (nullptr != _g)
            delete _g;

        if (nullptr != _basis)
            delete _basis;
    }

    AbstractBackFourierTransform &deleteMuGenerator();

    AbstractBackFourierTransform &setBasis(Basis *basis);
    AbstractBackFourierTransform &setMuGenerator(AbstractMuGenerator *generator);

    inline Basis *basis()
    {
        return _basis;
    }

    inline AbstractMuGenerator *muGenerator()
    {
        return _g;
    }

protected:
    struct Borders {
        int beginIdx;
        int endIdx;
    };

    inline Borders __getBorders(bool isBothSided, int N) const
    {
        auto beginIdx = (isBothSided) ? - N / 2 : 0;
        auto endIdx   = (isBothSided) ?   N / 2 : N;

        return {beginIdx, endIdx};
    }

    Basis               *_basis;
    AbstractMuGenerator *_g;
};


class StationaryBackFourierTransform : public AbstractBackFourierTransform
{
public:
    explicit inline StationaryBackFourierTransform(Basis               *basis     = nullptr,
                                                   AbstractMuGenerator *generator = nullptr):
        AbstractBackFourierTransform(basis, generator)
    {

    }

    virtual QVector<double> operator ()(TransformSpectrumStrategy::TransformedSpectrum &&spectrum,
                                        bool isBothSided = false);

    inline QVector<double> call(TransformSpectrumStrategy::TransformedSpectrum &&spectrum,
                                bool isBothSided = false)
    {
        return this->operator ()(std::move(spectrum), isBothSided);
    }
};


class NonstationaryBackFourierTransform : public AbstractBackFourierTransform
{
public:
    explicit inline NonstationaryBackFourierTransform(Basis *basis = nullptr,
                                                     AbstractMuGenerator *generator = nullptr):
        AbstractBackFourierTransform(basis, generator)
    {

    }

    QVector<double> operator ()(const QVector<double> &spectrum, bool isBothSided = false);

    inline QVector<double> call(const QVector<double> &spectrum, bool isBothSided = false)
    {
        return this->operator ()(spectrum, isBothSided);
    }
};


class StationaryBackHadamardTransfrom : public StationaryBackFourierTransform
{
public:
    inline StationaryBackHadamardTransfrom():
        StationaryBackFourierTransform(new HadamardBasis())
    {

    }

    explicit inline StationaryBackHadamardTransfrom(std::size_t N, AbstractMuGenerator *generator = nullptr):
        StationaryBackFourierTransform(new HadamardBasis(N), generator)
    {

    }

    StationaryBackHadamardTransfrom &setN(std::size_t N);
};


class NonstationaryBackHadamardTransform : public NonstationaryBackFourierTransform
{
public:
    inline NonstationaryBackHadamardTransform():
        NonstationaryBackFourierTransform(new HadamardBasis())
    {

    }

    explicit inline NonstationaryBackHadamardTransform(std::size_t N, AbstractMuGenerator *generator = nullptr):
        NonstationaryBackFourierTransform(new HadamardBasis(N), generator)
    {

    }

    NonstationaryBackHadamardTransform &setN(std::size_t N);
};

#endif // FOURIER_H
