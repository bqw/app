#ifndef MIXINS_H
#define MIXINS_H


class Uncopyable
{
public:
    inline Uncopyable()
    {

    }

    Uncopyable(const Uncopyable &rhs) = delete;

    virtual ~Uncopyable()
    {

    }

    const Uncopyable &operator =(const Uncopyable &rhs) = delete;
};


class Unmovable
{
public:
    inline Unmovable()
    {

    }

    Unmovable(Unmovable &&rhs) = delete;

    virtual ~Unmovable()
    {

    }

    Unmovable &&operator =(Unmovable &&rhs) = delete;
};

#endif // MIXINS_H
