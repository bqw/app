#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <cstring>
#include <iomanip>
#include <iostream>

#include <qmath.h>
#include <QVector>

#include "bases.h"
#include "density.h"
#include "generator.h"
#include "imitationexception.h"
#include "mixins.h"


/**
 * @brief The ComputeSpectrumStrategy class
 */
class ComputeSpectrumStrategy
{
public:
    explicit inline ComputeSpectrumStrategy(double timeInterval            = 0.0,
                                            AbstractDensity *density       = nullptr,
                                            const QVector<double> &lambdas = {}):
        _T(timeInterval),
        _S(density),
        _l(lambdas),
        _deltaW(__computeDeltaW()),
        _deltaT(__computeDeltaT()),
        _N(__computeN())
    {
        if (nullptr != _S && 0 == _S->cutOffFrequency())
            throw ImitationException("Не задана верхняя граничная частота ФСПМ");

        if (nullptr != _S && 0 == _S->maximum())
            throw ImitationException("Не задано максимальное значение ФСПМ");
    }

    virtual ~ComputeSpectrumStrategy();

    virtual QVector<double> operator ()(double wMaximum = 0.0);

    inline QVector<double> call(double wMaximum = 0.0)
    {
        return this->operator ()(wMaximum);
    }

    ComputeSpectrumStrategy &clearLambdas();

    ComputeSpectrumStrategy &setTimeInterval(double timeInterval);
    ComputeSpectrumStrategy &setDensity(AbstractDensity *density);
    ComputeSpectrumStrategy &setLambdas(const QVector<double> &lambdas);

    double inline timeInterval() const
    {
        return _T;
    }

    inline const AbstractDensity *density() const
    {
        return _S;
    }

    inline const QVector<double> &lambdas() const
    {
        return _l;
    }

    inline double deltaW() const
    {
        if (0.0 == _deltaW)
            _deltaW = __computeDeltaW();

        return _deltaW;
    }

    inline double deltaT() const
    {
        if (0.0 == _deltaT)
            _deltaT = __computeDeltaT();

        return _deltaT;
    }

    inline std::size_t N() const
    {
//        if (0 == _N)
//            _N = __computeN();

        return _N;
    }

protected:
    virtual std::size_t __computeN();

    inline void __checkDensity(AbstractDensity *density) const
    {
        if (density == _S)
            throw ImitationException("Не задана ФСПМ для вычисления спектра.");

        if (0 == density->cutOffFrequency())
            throw ImitationException("Не задана верхняя граничная частота ФСПМ");

        if (0 == density->maximum())
            throw ImitationException("Не задано максимальное значение ФСПМ");
    }

    inline double __computeDeltaW() const
    {
        return (0 == _T) ? 0.0 : 2 * M_PI / _T;
    }

    inline double __computeDeltaT() const
    {
        return (nullptr == _S) ? 0.0 : M_PI / _S->cutOffFrequency();
    }

    double              _T;
    AbstractDensity    *_S;
    QVector<double>     _l;
    mutable double      _deltaW;
    mutable double      _deltaT;
    mutable std::size_t _N;
};


class ComputeSpectrumStrategyHadamard : public ComputeSpectrumStrategy
{
public:
    explicit inline ComputeSpectrumStrategyHadamard(double timeInterval            = 0.0,
                                                    AbstractDensity *density       = nullptr,
                                                    const QVector<double> &lambdas = {}):
        ComputeSpectrumStrategy(timeInterval, density, lambdas)
    {
        _N = __computeN();
    }

protected:
    std::size_t __computeN() override;
};


class TransformSpectrumStrategy : public Uncopyable
{
public:
    struct TransformedSpectrum
    {
        std::size_t rowSize;
        std::size_t fullSize;
        double    **data;

        TransformedSpectrum(std::size_t rowSize_, std::size_t fullSize_, double **data_):
            rowSize(rowSize_),
            fullSize(fullSize_),
            data(data_)
        {

        }

        TransformedSpectrum(TransformedSpectrum &&rhs):
            rowSize(rhs.rowSize),
            fullSize(rhs.fullSize),
            data(rhs.data)
        {
            rhs.data = nullptr;
        }

        ~TransformedSpectrum()
        {
            if (nullptr == data)
                return;

            for (std::size_t i = 0; i < rowSize; i++) {
                if (nullptr == data[i])
                    continue;

                delete [] data[i];
            }

            delete [] data;
        }

        inline QVector<double> toVector() const
        {
            QVector<double> spectrum;
            spectrum.reserve(rowSize);

            for (std::size_t i = 0; i < rowSize; i++) {
                auto tmp = 0.0;

                for (std::size_t j = 0; j < rowSize; j++)
                    tmp += (fabs(data[i][j]) > 10e-10) ? data[i][j] : 0.0;

                spectrum << tmp;
            }

            return spectrum;
        }
    };

    explicit inline TransformSpectrumStrategy(Basis *currentBasis = nullptr,
                                              Basis *targetBasis = nullptr):
        _N(0),
        _kernel(nullptr),
        _currentBasis(currentBasis),
        _targetBasis(targetBasis),
        _changed(true)
    {
        if (nullptr != _currentBasis && nullptr != _targetBasis) {
            if (_currentBasis->N() != _targetBasis->N())
                throw ImitationException("Базисы не могут быть преобразованы друг в друга - разное количество точек разбиения");
            else
                setN(_currentBasis->N());

            return;
        }

        if (nullptr != _currentBasis)
            setN(currentBasis->N());

        if (nullptr != _targetBasis)
            setN(_targetBasis->N());
    }

    ~TransformSpectrumStrategy();

    TransformedSpectrum operator ()(const QVector<double> &spectrum);

    inline TransformedSpectrum call(const QVector<double> &spectrum)
    {
        return this->operator ()(spectrum);
    }

    TransformSpectrumStrategy &init();

    TransformSpectrumStrategy &setN(std::size_t N);
    TransformSpectrumStrategy &setCurrentBasis(Basis *currentBasis);
    TransformSpectrumStrategy &setTargetBasis(Basis *targetBasis);

    inline std::size_t N() const
    {
        return _N;
    }

    inline const Basis *currentBasis() const
    {
        return _currentBasis;
    }

    inline const Basis *targetBasis() const
    {
        return _targetBasis;
    }

private:
    inline double **__allocate()
    {
        try {
            double **data = new double* [_N];

            for (std::size_t i = 0; i < _N; i++) {
                data[i] = new double [_N];
                std::memset(data[i], 0, _N * sizeof(data[i][0]));
            }

            return data;
        }
        catch (std::bad_alloc &) {
            throw ImitationException("Невозможно выделить память для ядра преобразования спектров");
        }
    }

    inline void __deleteKernel()
    {
        if (nullptr == _kernel)
            return;

        for (std::size_t i = 0; i < _N; i++) {
            if (nullptr == _kernel[i])
                continue;

            delete [] _kernel[i];
        }

        delete [] _kernel;
    }

    inline void __computeKernel()
    {
//        std::cout << std::endl;

//        for (auto i = 0; i < _N; i++) {
//            for (auto j = 0; j < _N; j++)
//                std::cout << _kernel[i][j] << " ";

//            std::cout << std::endl;
//        }

//        std::cout << std::endl;

        QVector<std::size_t> functionNumbers;
        functionNumbers.reserve(_N);

        functionNumbers << 0;

        for (std::size_t i = 1; i < _N / 2; i++)
            functionNumbers << i << i;

        functionNumbers << _N / 2;

        for (std::size_t i = 0; i < _N; i++)
            std::memset(_kernel[i], 0, _N);

        _kernel[0][0]      = 1.;
        _kernel[1][_N - 1] = 1.;

        auto n = static_cast<std::size_t>(floor(log2(_N)));

        for (std::size_t gamma = 3; gamma <= n + 1; gamma++) {
            std::size_t nGroupElements = pow(2, gamma - 2);
            std::size_t factor         = pow(2, n + 1 - gamma);

            for (std::size_t j = 0; j < nGroupElements; j++) {
                std::size_t k = nGroupElements + j;

                for (std::size_t m = 0; m < pow(2, gamma - 3); m++) {
                    // Номер функции в тригонометрическом базисе (от 0 до N / 2 - 1).
                    std::size_t functionNo  = factor * (1 + 2 * m);
                    // Порядковый номер функции (от 0 до N - 1).
                    std::size_t functionPos = pow(2, (n + 2 - gamma)) * (1 + 2 * m);

                    auto tmpEven = 0.0;
                    auto tmpOdd  = 0.0;

                    for (std::size_t i = 0; i < _N; i++) {
                        tmpEven += _targetBasis->at(k)(k, i) * _currentBasis->at(functionPos - 1)(functionNo, i);
                        tmpOdd  += _targetBasis->at(k)(k, i) * _currentBasis->at(functionPos)(functionNo, i);
                    }

                    _kernel[k][functionPos - 1] = tmpEven / _N;
                    _kernel[k][functionPos]     = tmpOdd / _N;
                }
            }
        }

//        for (std::size_t k = 0; k < _N; k++) {
//            std::memset(_kernel[k], 0, _N);

//            for (std::size_t m = 0; m < _N; m++) {
//                auto tmpSum = 0.0;

//                for (std::size_t i = 0; i < _N; i++)
//                    tmpSum += _targetBasis->at(k)(k, i) * _currentBasis->at(m)(functionNumbers[m], i);

//                _kernel[k][m] = (10e-10 < fabs(tmpSum)) ? tmpSum / _N : 0.0;
//            }
//        }
    }

    std::size_t _N;

    double **_kernel;

    Basis *_currentBasis;
    Basis *_targetBasis;

    bool _changed;
};

#endif // SPECTRUM_H
