/****************************************************************************
**
** Описания функций спектральной плотности мощности (ФСПМ) различных сигналов.
**
****************************************************************************/

#ifndef DENSITY_H
#define DENSITY_H

#include <qmath.h>


/**
 * @brief Абстрактный класс ФСПМ.
 */
class AbstractDensity
{
public:
    /**
     * @brief Конструктор.
     * @param cutOffFrequency Верхняя граничная частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline AbstractDensity(double cutOffFrequency = 0.0, double densityMaximum = 0.0):
        _w_h(fabs(cutOffFrequency)),
        _S(fabs(densityMaximum))
    {

    }

    /**
     * @brief Виртуальный деструктор иерархии.
     */
    virtual ~AbstractDensity();

    /**
     * @brief Операция вычисления значения ФСПМ для отпределенного значения частоты.
     * @param w Значение циклической частоты.
     * @return
     */
    virtual double operator ()(double w) = 0;

    /**
     * @brief Вызывает операцию вычисления значения ФСПМ.
     * Введено для более удобной работы с объектом через указатель.
     * @param w
     * @return
     */
    inline double call(double w)
    {
        return this->operator ()(w);
    }

    /**
     * @brief Сеттер верхней граничной циклической частоты.
     * @param cutOffFrequency Верхняя граничная частота.
     * @return
     */
    virtual AbstractDensity &setCutOffFrequency(double cutOffFrequency);
    /**
     * @brief Сеттер максимального значения ФСПМ.
     * @param densityMaximum Максимальное значение ФСПМ.
     * @return
     */
    virtual AbstractDensity &setMaximum(double densityMaximum);

    /**
     * @brief Возвращает значение верхней граничной частоты.
     * @return
     */
    inline double cutOffFrequency() const noexcept
    {
        return _w_h;
    }

    /**
     * @brief Возвращает максимальное значение ФСПМ.
     * @return
     */
    inline double maximum() const noexcept
    {
        return _S;
    }

protected:
    /**
     * @brief Верхняя граничная циклическая частота.
     */
    double _w_h;
    /**
     * @brief Максимальное значение ФСПМ.
     */
    double _S;
};


/**
 * @brief ФСПМ физического белого шума (ограниченного частотой среза).
 */
class WhiteNoiseDensity : public AbstractDensity
{
public:
    /**
     * @brief Конструктор.
     * @param cutOffFrequency Верхняя граничная частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline WhiteNoiseDensity(double cutOffFrequency = 0.0, double densityMaximum = 0.0):
        AbstractDensity(cutOffFrequency, densityMaximum)
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения ФСПМ для отпределенного значения частоты.
     * @param w Значение циклической частоты.
     * @return
     */
    double operator ()(double w) override;
};


/**
 * @brief ФСПМ треугольной формы.
 */
class TriangularDensity : public AbstractDensity
{
public:
    /**
     * @brief Конструктор с параметрами.
     * @param cutOffFrequency Верхняя граничная частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline TriangularDensity(double cutOffFrequency = 0.0, double densityMaximum = 0.0):
        AbstractDensity(cutOffFrequency, densityMaximum)
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения ФСПМ для отпределенного значения частоты.
     * @param w Значение циклической частоты.
     * @return
     */
    double operator ()(double w) override;
};


/**
 * @brief ФСПМ экспоненциальной формы.
 */
class ExponentialDensity : public AbstractDensity
{
public:
    /**
     * @brief Конструктор.
     * @note  Вычисляет дополнительные поля.
     * @param cutOffFrequency Верхняя граничная частота.
     * @param densityMaximum  Максимальное значение ФСПМ.
     */
    explicit inline ExponentialDensity(double cutOffFrequency = 0.0, double densityMaximum = 0.0):
        AbstractDensity(cutOffFrequency, densityMaximum),
        _w_h_2(pow(_w_h, 2))
    {

    }

    /**
     * @brief Перегруженная операция вычисления значения ФСПМ для отпределенного значения частоты.
     * @param w Значение циклической частоты.
     * @return
     */
    double operator ()(double w) override;

    /**
     * @brief Сеттер верхней граничной циклической частоты.
     * @param cutOffFrequency Верхняя граничная частота.
     * @return
     */
    ExponentialDensity &setCutOffFrequency(double cutOffFrequency) override;

protected:
    /**
     * @brief Квадрат верхней граничной циклической частоты.
     */
    double _w_h_2;
};

#endif // DENSITY_H
