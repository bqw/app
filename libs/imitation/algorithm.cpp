#include "algorithm.h"


AbstractAlgorithm::AbstractAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy,
                                     TransformSpectrumStrategy    *transformSpectrumStrategy,
                                     AbstractBackFourierTransform *fourierTransform):
    _computeSpec(computeSpectrumStrategy),
    _transformSpec(transformSpectrumStrategy),
    _computeSignal(fourierTransform),
    _needsInitialize(true)
{

}

AbstractAlgorithm::~AbstractAlgorithm()
{
    if (nullptr != _computeSpec)
        delete _computeSpec;

    if (nullptr != _transformSpec)
        delete _transformSpec;

    if (nullptr != _computeSignal)
        delete _computeSignal;
}

AbstractAlgorithm &AbstractAlgorithm::setComputeSpectrumStrategy(ComputeSpectrumStrategy *computeSpectrumStrategy)
{
    if (nullptr != _computeSpec)
        delete _computeSpec;

    _computeSpec     = computeSpectrumStrategy;
    _needsInitialize = true;

    return *this;
}

AbstractAlgorithm &AbstractAlgorithm::setTransfromSpectrumStrategy(TransformSpectrumStrategy *transformSpectrumStrategy)
{
    if (nullptr != _transformSpec)
        delete _transformSpec;

    _transformSpec   = transformSpectrumStrategy;
    _needsInitialize = true;

    return *this;
}

AbstractAlgorithm &AbstractAlgorithm::setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform)
{
    if (nullptr != _computeSignal)
        delete _computeSignal;

    _computeSignal   = backFourierTransform;
    _needsInitialize = true;

    return *this;
}

AbstractAlgorithm &AbstractAlgorithm::setIsBothSided(bool isBothSided)
{
    _isBothSided = isBothSided;
    return *this;
}

AbstractAlgorithm& AbstractAlgorithm::setParams(AlgorithmParams &&params)
{
    _params = std::move(params);

    return *this;
}

void AbstractAlgorithm::init()
{
    try {
        __checkInitialized();

        setIsBothSided(_params.isBothSided);

        _computeSpec->setTimeInterval(_params.timeInterval);
        _computeSpec->setDensity(_params.density);
        _computeSpec->setLambdas(_params.lambdas);

        _transformSpec->setN(_computeSpec->N());

        if (nullptr != _params.generator)
            _computeSignal->setMuGenerator(_params.generator);

        _computeSignal->basis()->setN(_computeSpec->N());

        _transformSpec->init();

        _needsInitialize = false;

        emit initializationFinished();
    }
    catch (QException &exc) {
        qDebug() << exc.what();
        emit errorOccured(exc.what());
    }
    catch (...) {
        return;
    }
}


StationaryAlgorithm::StationaryAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy,
                                         TransformSpectrumStrategy    *transformSpectrumStrategy,
                                         AbstractBackFourierTransform *fourierTransform):
    AbstractAlgorithm(computeSpectrumStrategy, transformSpectrumStrategy, fourierTransform)
{
    if (nullptr != fourierTransform && nullptr == dynamic_cast<StationaryBackFourierTransform *>(fourierTransform))
        throw ImitationException("Неверно задан тип преобразования Фурье");
}

StationaryAlgorithm &StationaryAlgorithm::setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform)
{
    if (nullptr == dynamic_cast<StationaryBackFourierTransform *>(backFourierTransform))
        throw ImitationException("Неверно задан тип преобразования Фурье");

    if (nullptr == _computeSignal)
        delete _computeSignal;

    _computeSignal = backFourierTransform;

    return *this;
}

QVector<QPointF> StationaryAlgorithm::implement()
{
    try {
        auto transform = dynamic_cast<StationaryBackFourierTransform *>(_computeSignal);

        if (nullptr == transform)
            throw ImitationException("Невозможно осуществить обратное преобразование Фурье - задан неверный тип преобразования");

        auto spectrum = __prepareSpectrum();

        auto transformed = _transformSpec->call(spectrum);

        auto signal   = transform->call(std::move(transformed), _isBothSided);

        auto points = __toPoints(signal);

        emit imitationFinished();
        emit imitationFinished(points);

        return points;
    }
    catch (QException &exc) {
        qDebug() << exc.what();
        emit errorOccured(exc.what());
    }
    catch (...) {
        return {};
    }

    return {};
}


NonstationaryAlgorithm::NonstationaryAlgorithm(ComputeSpectrumStrategy      *computeSpectrumStrategy,
                                               TransformSpectrumStrategy    *transformSpectrumStrategy,
                                               AbstractBackFourierTransform *fourierTransform):
    AbstractAlgorithm(computeSpectrumStrategy, transformSpectrumStrategy, fourierTransform)
{
    if (nullptr != fourierTransform && nullptr == dynamic_cast<NonstationaryBackFourierTransform *>(_computeSignal))
        throw ImitationException("Неверно задан тип преобразования Фурье");
}

NonstationaryAlgorithm &NonstationaryAlgorithm::setFourierTransformStrategy(AbstractBackFourierTransform *backFourierTransform)
{
    if (nullptr == dynamic_cast<NonstationaryBackFourierTransform *>(backFourierTransform))
        throw ImitationException("Неверно задан тип преобразования Фурье");

    if (nullptr != _computeSignal)
        delete _computeSignal;

    _computeSignal = backFourierTransform;

    return *this;
}

QVector<QPointF> NonstationaryAlgorithm::implement()
{
    try {
        auto transform = dynamic_cast<NonstationaryBackFourierTransform *>(_computeSignal);

        if (nullptr == transform)
            throw ImitationException("Невозможно осуществить обратное преобразование Фурье - задан неверный тип преобразования");

        auto spectrum = __prepareSpectrum();
        auto signal   = transform->call(_transformSpec->call(spectrum).toVector(), _isBothSided);

        auto points = __toPoints(signal);

        emit imitationFinished();
        emit imitationFinished(points);

        return points;
    }
    catch (QException &exc) {
        qDebug() << exc.what();
        emit errorOccured(exc.what());
    }
    catch (...) {
        return {};
    }

    return {};
}
