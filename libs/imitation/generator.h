#ifndef GENERATOR_H
#define GENERATOR_H

#include <qmath.h>
#include <QVector>

#include <cstdlib>
#include <numeric>
#include <random>

#include "imitationexception.h"


class AbstractGenerator
{
public:
    inline AbstractGenerator()
    {

    }

    virtual ~AbstractGenerator()
    {

    }

    virtual double operator ()() = 0;

    inline double get()
    {
        return this->operator ()();
    }

protected:
    std::random_device _dev;
};


/**
 * @brief The LambdaGeneratorBase class
 */
class AbstractLambdaGenerator : public AbstractGenerator
{
public:
    inline AbstractLambdaGenerator():
        AbstractGenerator()
    {

    }

    virtual QVector<double> operator ()(std::size_t length);

    inline double get()
    {
        return AbstractGenerator::get();
    }

    inline QVector<double> get(std::size_t length)
    {
        return this->operator ()(length);
    }
};


class LambdaGeneratorNormal : public AbstractLambdaGenerator
{
public:
    explicit inline LambdaGeneratorNormal(double mu = 0.0, double gamma = 0.0):
        _dist(mu, gamma)
    {

    }

    double operator ()() override;

private:
    std::normal_distribution<double> _dist;
};


/**
 * @brief The MuGeneratorBase class
 */
class AbstractMuGenerator : public AbstractGenerator
{
public:
    inline AbstractMuGenerator():
        AbstractGenerator()
    {

    }

    virtual QVector<QPair<double, double> > operator ()(std::size_t length) = 0;

    inline double get()
    {
        return AbstractGenerator::get();
    }

    inline QVector<QPair<double, double> > get(std::size_t length)
    {
        return this->operator ()(length);
    }
};


class MuGeneratorUniform : public AbstractMuGenerator
{
public:
    explicit inline MuGeneratorUniform(int min = -1, int max = 1):
        _dist(min, max)
    {

    }

    double          operator ()() override;
    QVector<QPair<double, double> > operator ()(std::size_t length) override;

private:
    static auto constexpr BORDER = 0.0;

    std::uniform_real_distribution<double> _dist;
};

#endif // GENERATOR_H
