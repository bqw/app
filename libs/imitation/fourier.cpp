#include "fourier.h"


AbstractBackFourierTransform &AbstractBackFourierTransform::deleteMuGenerator()
{
    if (nullptr != _g) {
        delete _g;
        _g = nullptr;
    }

    return *this;
}

AbstractBackFourierTransform &AbstractBackFourierTransform::setBasis(Basis *basis)
{
    if (nullptr != _basis)
        delete _basis;

    _basis = basis;

    return *this;
}

AbstractBackFourierTransform &AbstractBackFourierTransform::setMuGenerator(AbstractMuGenerator *generator)
{
    if (nullptr != _g)
        delete _g;

    _g = generator;

    return *this;
}


QVector<double> StationaryBackFourierTransform::operator ()(TransformSpectrumStrategy::TransformedSpectrum &&spectrum,
                                                            bool isBothSided)
{
    if (nullptr == _basis)
        throw ImitationException("Базис обратного преобразования Фурье не инициализирован");

    if (spectrum.rowSize != _basis->N())
        _basis->setN(spectrum.rowSize);

    if (0 == spectrum.rowSize || 0 == spectrum.fullSize)
        return {};

    QVector<double> signal;
    signal.reserve(spectrum.rowSize);

    // Вычисление границ изменения индекса i (номер отсчета сигнала).
    auto borders = __getBorders(isBothSided, spectrum.rowSize);

    // Генератор равен nullptr => детерминированный сигнал.
    if (nullptr == _g) {
        // Так как воспроизводится детерминированный сигнал и случайные знаки менять не нужно,
        // можно получить детерминированные коэффициенты путем сложения элементов в каждой строке
        // преобразованного спектра (в такой матрице спектральный коэффициент Адамара представлен
        // в виде суммы спектральных коэффициентов базиса Фурье).

        auto spectrumBuffer = spectrum.toVector();

        for (auto i = borders.beginIdx; i < borders.endIdx; i++) {
            auto k = 1;

            signal << std::accumulate(spectrumBuffer.cbegin() + 2, spectrumBuffer.cend(),
                                      spectrumBuffer[0] * _basis->at(0)(0, i) + spectrumBuffer[1] * _basis->at(1)(1, i),
                                      [this, k, i](double init, double elem) mutable {
                                          return (fabs(elem) > 10e-10) ? init + elem * _basis->at(++k)(k, i) : init;
                                      });
        }
    }
    // Иначе случайный.
    else {
        // Вычисление спектра Адамара с учетом случайных составляющих.
        QVector<double> hadamardSpectrum;
        hadamardSpectrum.reserve(spectrum.rowSize);

        hadamardSpectrum << _g->get() * spectrum.data[0][0];
        hadamardSpectrum << _g->get() * spectrum.data[1][spectrum.rowSize - 1];

        for (auto i = static_cast<std::size_t>(hadamardSpectrum.size()); i < spectrum.rowSize; i++) {
            auto tmp = 0.0;
            for (std::size_t j = 0; j < spectrum.rowSize; j++)
                tmp += (fabs(spectrum.data[i][j]) > 10e-10) ? _g->get() * spectrum.data[i][j] : 0.0;

            if (fabs(tmp) > 10e10)
                tmp = 0.0;

            hadamardSpectrum << tmp;
        }

        for (auto i = borders.beginIdx; i < borders.endIdx; i++) {
            auto k = 1;

            signal << std::accumulate(hadamardSpectrum.cbegin() + 2, hadamardSpectrum.cend(),
                                      hadamardSpectrum[0] * _basis->at(0)(0, i) + hadamardSpectrum[1] * _basis->at(1)(1, i),
                                      [this, k, i](double init, double elem) mutable {
                                          return (fabs(elem) > 10e-10) ? init + elem * _basis->at(++k)(k, i) : init;
                                      });
        }
    }

    return signal;
}


QVector<double> NonstationaryBackFourierTransform::operator ()(const QVector<double> &spectrum,
                                                               bool isBothSided)
{
    if (nullptr == _basis)
        throw ImitationException("Базис обратного преобразования Фурье не инициализирован");

    if (static_cast<std::size_t>(spectrum.size()) != _basis->N())
        _basis->setN(spectrum.size());

    if (spectrum.isEmpty())
        return {};

    QVector<double> signal;
    signal.reserve(spectrum.size());

    auto borders = __getBorders(isBothSided, spectrum.size());

    if (nullptr == _g) {
        for (auto i = borders.beginIdx; i < borders.endIdx; i++) {
            auto k = 1;

            signal << std::accumulate(spectrum.cbegin() + 2, spectrum.cend(),
                                      spectrum[0] * _basis->at(0)(0, i) + spectrum[1] * _basis->at(1)(1, i),
                                      [this, k, i](double init, double elem) mutable {
                                          return (fabs(elem) > 10e-10) ? init + elem * _basis->at(++k)(k, i) : init;
                                      });
        }
    }
    else {
        for (auto i = borders.beginIdx; i < borders.endIdx; i++) {
            auto k = 1;

            // TODO: Исправить.
            signal << std::accumulate(spectrum.cbegin() + 2, spectrum.cend(),
                                      _g->get() * spectrum[0] * _basis->at(0)(0, i) + _g->get() * spectrum[1] * _basis->at(1)(1, i),
                                      [this, k, i](double init, double elem) mutable {
                                          return (fabs(elem) > 10e-10) ? init + _g->get() * elem * _basis->at(++k)(k, i) : init;
                                      });
        }
    }

    return signal;
}


StationaryBackHadamardTransfrom &StationaryBackHadamardTransfrom::setN(std::size_t N)
{
    if (nullptr != _basis)
        _basis->setN(N);

    return *this;
}


NonstationaryBackHadamardTransform &NonstationaryBackHadamardTransform::setN(std::size_t N)
{
    if (nullptr != _basis)
        _basis->setN(N);

    return *this;
}
